<?php
function mg_encrypt($msg)
{

    $string = $msg;
    return urlencode(base64_encode($string));
}

function mg_decrypt($msg)
{
    $string = base64_decode(urldecode($msg));
    return $string;
}

add_action("wp_ajax_mg_receive_user_data", "mg_receive_user_data");
add_action( 'wp_ajax_nopriv_mg_receive_user_data', 'mg_receive_user_data' );

function mg_receive_user_data()
{


    $mg_email = sanitize_email($_POST['mg_email']);
    $mg_full_name = strip_tags($_POST['mg_full_name']);
    $mg_targeturl = strip_tags($_POST['targeturl']);

    if(strpos($mg_targeturl,'?') !== false){
    $mg_targeturl = explode('?',$mg_targeturl)[0];
    }

    $user = get_user_by( 'email', $mg_email );

    if($user->ID > 0 && $mg_email != 'araphp1@gmail.com'){

    echo 'found';

    }else{

    $date = date('Y-m-d h A');
    $id = wp_insert_post(array('post_title'=> $mg_full_name.' - New lead at '.$date, 'post_type'=>'mg_leads', 'post_content'=>''));
    update_field('first_name',$mg_full_name,$id);
    update_field('email',$mg_email,$id);

    // the message
    $otmg = mg_encrypt($mg_email . 'codesign' . $mg_full_name);
    // send email
    ob_start();
    include(ABSPATH.'/wp-content/themes/buyablebusiness/inc/email-templates/confirmation.php');
    $body = ob_get_clean();
    //$body = "Confirm your email on Buyable Businesss by click the link below:\n" . site_url() . "/confirmation/?otmg=" . $otmg;
    $headers = array(
        'Content-Type: text/html; charset=UTF-8'
    );

    wp_mail($mg_email, "The Buyability Toolkit Email Confirmation", $body, $headers);


    echo 'Success';

}
    exit;
}

/* Custom Post Type Start */
function mg_posttype() {
register_post_type( 'mg_leads',
// CPT Options
array(
'labels' => array(
'name' => __( 'Form Leads' ),
'singular_name' => __( 'Form Lead' )
),
'public' => true,
'has_archive' => false,
'rewrite' => array('slug' => 'mg-form-leads'),
)
);
}
// Hooking up our function to theme setup
add_action( 'init', 'mg_posttype' );



function allow_programmatic_login( $user, $username, $password ) {
    return get_user_by( 'login', $username );
 }

function programmatic_login( $username ) {
        if ( is_user_logged_in() ) {
            wp_logout();
        }

    add_filter( 'authenticate', 'allow_programmatic_login', 10, 3 );    // hook in earlier than other callbacks to short-circuit them
    $user = wp_signon( array( 'user_login' => $username ) );
    remove_filter( 'authenticate', 'allow_programmatic_login', 10, 3 );

    if ( is_a( $user, 'WP_User' ) ) {
        wp_set_current_user( $user->ID, $user->user_login );

        if ( is_user_logged_in() ) {
            return true;
        }
    }

    return false;
 }

// function mg_auto_login(){
// if(isset($_GET['otmg']) && isset($_GET['um_form_id'])):
//   $otmg = mg_decrypt($_GET['otmg']);
//   $exp = explode('codesign', $otmg);
//   $mg_full_name = $exp[1];
//   $mg_email = $exp[0];

//   $data = get_user_by('email',$mg_email);
//   $id = $data->ID;
//     programmatic_login( $data->user_login );
//         //login
//         wp_set_current_user($id, $data->user_login);
//         wp_set_auth_cookie($id);
//         do_action('wp_login', $data->user_login);

//     $redirect_to = explode("?","https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]")[0];
//     wp_safe_redirect( $redirect_to );
//     exit;
// endif;

// }
//add_action('wp_head','mg_auto_login');

add_role( 'buyable_upgraded', 'Buyable user', get_role( 'subscriber' )->capabilities );

function mg_show_conf_on_upgrade(){

    if(isset($_GET['upgrade']) && $_GET['upgrade'] == 1){
        ?>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    jQuery(document).ready(function(){
            Swal.fire({
          icon: 'success',
          title: 'Welcome as a Premium User of The Buyability Toolkit!',
          text: `  Enjoy your tools and please tell us what you like, what needs to improve, and what you are missing, by writing to support@buyablebusiness.com`,
          showConfirmButton: false,
          timer: 3500
        });
        });
</script>
    <style>
      .swal2-show {
        display: grid;
        width: 50%;
        font-size: 16px;
      }

      .swal2-input, .swal2-file {
        display: none !important;
      }
    </style>
        <?php
    }


}

add_action('wp_head','mg_show_conf_on_upgrade');


function mg_send_req_update(){
    global $current_user;


    $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if(strpos($actual_link,'/password-reset/') !== false && is_user_logged_in()){
        header('Location: '.site_url());
        exit;
    }


  if (isset($_POST['first_name-2468'])) {
    foreach ($_POST as $key => $val) {
      if (strpos($key, '-2468') !== false) {
        $meta_key = explode('-2468', $key)[0];
        $meta_value = $val;

        update_user_meta($current_user->ID, $meta_key, $meta_value);

      }
    }
//exit;
  }
}

add_action('init','mg_send_req_update');

add_filter('um_account_page_default_tabs_hook', 'mg_custom_tab_in_um', 100 );
function mg_custom_tab_in_um( $tabs ) {
    global $current_user;
       $user_meta=get_userdata($current_user->ID);

       $user_roles=$user_meta->roles;

       if(in_array('buyable_upgraded',$user_roles)){
    $tabs[800]['acc-information']['icon'] = 'um-faicon-pencil';
    $tabs[800]['acc-information']['title'] = 'Edit Account Information';
    $tabs[800]['acc-information']['custom'] = true;
    }

    return $tabs;
}

/* make our new tab hookable */

add_action('um_account_tab__mytab', 'um_account_tab__mytab');
function um_account_tab__mytab( $info ) {
    global $ultimatemember;
    extract( $info );

    $output = $ultimatemember->account->get_tab_output('acc-information');
    if ( $output ) { echo $output; }
}

/* Finally we add some content in the tab */

add_filter('um_account_content_hook_acc-information', 'um_account_content_hook_mytab');
function um_account_content_hook_mytab( $output ){
    global $current_user;
  //var_dump($_POST);
  if (isset($_POST['first_name-2468'])) {
    foreach ($_POST as $key => $val) {
      if (strpos($key, '-2468') !== false) {
        $meta_key = explode('-2468', $key)[0];
        $meta_value = $val;

        update_user_meta($current_user->ID, $meta_key, $meta_value);

      }
    }
exit;
  }

    ob_start();
    ?>

    <div class="um-field">

<div class="um um-register um-2468">

        <div class="um-form" data-mode="register">

          <form method="post">

            <div id="um_field_2468_first_name" class="um-field um-field-text  um-field-first_name um-field-text um-field-type_text" data-key="first_name">
              <div class="um-field-label"><label for="first_name-2468">Full
                  Name</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid " type="text" name="first_name-2468" id="first_name-2468" value="<?= get_user_meta($current_user->ID, 'first_name', true); ?>" placeholder="" data-validate="" data-key="first_name"/>

              </div>
            </div>
            <div id="um_field_2468_phone_number" class="um-field um-field-text  um-field-phone_number um-field-text um-field-type_text" data-key="phone_number">
              <div class="um-field-label">
                <div class="um-field-label-icon">
                  <i class="um-faicon-phone" aria-label="Phone Number"></i>
                </div>
                <label for="phone_number-2468">Phone Number</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid " type="text" name="phone_number-2468" id="phone_number-2468" value="<?= get_user_meta($current_user->ID, 'phone_number', true); ?>" placeholder="" data-validate="phone_number" data-key="phone_number"/>

              </div>
            </div>
            <div id="um_field_2468_address" class="um-field um-field-text  um-field-address um-field-text um-field-type_text" data-key="address">
              <div class="um-field-label">
                <label for="address-2468">Address</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="address-2468" id="address-2468" value="<?= get_user_meta($current_user->ID, 'address', true); ?>" placeholder="Address" data-validate="" data-key="address"/>

              </div>
            </div>
            <div id="um_field_2468_city" class="um-field um-field-text  um-field-city um-field-text um-field-type_text" data-key="city">
              <div class="um-field-label"><label for="city-2468">city</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="city-2468" id="city-2468" value="<?= get_user_meta($current_user->ID, 'city', true); ?>" placeholder="city" data-validate="" data-key="city"/>

              </div>
            </div>
            <div id="um_field_2468_Zip_Code" class="um-field um-field-text  um-field-Zip_Code um-field-text um-field-type_text" data-key="Zip_Code">
              <div class="um-field-label"><label for="Zip_Code-2468">Zip
                  Code</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="Zip_Code-2468" id="Zip_Code-2468" value="<?= get_user_meta($current_user->ID, 'Zip_Code', true); ?>" placeholder="Zip Code" data-validate="" data-key="Zip_Code"/>

              </div>
            </div>
            <div id="um_field_2468_State" class="um-field um-field-text  um-field-State um-field-text um-field-type_text" data-key="State">
              <div class="um-field-label"><label for="State-2468">State</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="State-2468" id="State-2468" value="<?= get_user_meta($current_user->ID, 'State', true); ?>" placeholder="State" data-validate="" data-key="State"/>

              </div>
            </div>
            <div id="um_field_2468_country" class="um-field um-field-select  um-field-country um-field-select um-field-type_select" data-key="country">
              <div class="um-field-label">
                <label for="country-2468">Country</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area  ">
                <select data-default="" name="country-2468" id="country" data-validate="" data-key="country" class="um-form-field valid um-s1 " style="width: 100%" data-placeholder="Choose a Country">
                  <option value=""></option>
                  <?php
                  if (get_user_meta($current_user->ID, 'country', true) != '') {
                    ?>
                    <option value="<?= get_user_meta($current_user->ID, 'country', true); ?>" selected><?= get_user_meta($current_user->ID, 'country', true); ?></option>
                    <?php
                  }
                  ?>
                  <option value="United States">United States</option>
                  <option value="Afghanistan">Afghanistan</option>
                  <option value="Åland Islands">Åland Islands</option>
                  <option value="Albania">Albania</option>
                  <option value="Algeria">Algeria</option>
                  <option value="American Samoa">American Samoa</option>
                  <option value="Andorra">Andorra</option>
                  <option value="Angola">Angola</option>
                  <option value="Anguilla">Anguilla</option>
                  <option value="Antarctica">Antarctica</option>
                  <option value="Antigua and Barbuda">Antigua and Barbuda
                  </option>
                  <option value="Argentina">Argentina</option>
                  <option value="Armenia">Armenia</option>
                  <option value="Aruba">Aruba</option>
                  <option value="Australia">Australia</option>
                  <option value="Austria">Austria</option>
                  <option value="Azerbaijan">Azerbaijan</option>
                  <option value="Bahamas">Bahamas</option>
                  <option value="Bahrain">Bahrain</option>
                  <option value="Bangladesh">Bangladesh</option>
                  <option value="Barbados">Barbados</option>
                  <option value="Belarus">Belarus</option>
                  <option value="Belgium">Belgium</option>
                  <option value="Belize">Belize</option>
                  <option value="Benin">Benin</option>
                  <option value="Bermuda">Bermuda</option>
                  <option value="Bhutan">Bhutan</option>
                  <option value="Bolivia, Plurinational State of">Bolivia,
                    Plurinational State of
                  </option>
                  <option value="Bosnia and Herzegovina">Bosnia and
                    Herzegovina
                  </option>
                  <option value="Botswana">Botswana</option>
                  <option value="Bouvet Island">Bouvet Island</option>
                  <option value="Brazil">Brazil</option>
                  <option value="British Indian Ocean Territory">British Indian
                    Ocean Territory
                  </option>
                  <option value="Brunei Darussalam">Brunei Darussalam</option>
                  <option value="Bulgaria">Bulgaria</option>
                  <option value="Burkina Faso">Burkina Faso</option>
                  <option value="Burundi">Burundi</option>
                  <option value="Cambodia">Cambodia</option>
                  <option value="Cameroon">Cameroon</option>
                  <option value="Canada">Canada</option>
                  <option value="Cape Verde">Cape Verde</option>
                  <option value="Cayman Islands">Cayman Islands</option>
                  <option value="Central African Republic">Central African
                    Republic
                  </option>
                  <option value="Chad">Chad</option>
                  <option value="Chile">Chile</option>
                  <option value="China">China</option>
                  <option value="Christmas Island">Christmas Island</option>
                  <option value="Cocos (Keeling) Islands">Cocos (Keeling)
                    Islands
                  </option>
                  <option value="Colombia">Colombia</option>
                  <option value="Comoros">Comoros</option>
                  <option value="Congo">Congo</option>
                  <option value="Congo, the Democratic Republic of the">Congo,
                    the Democratic Republic of the
                  </option>
                  <option value="Cook Islands">Cook Islands</option>
                  <option value="Costa Rica">Costa Rica</option>
                  <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                  <option value="Croatia">Croatia</option>
                  <option value="Cuba">Cuba</option>
                  <option value="Cyprus">Cyprus</option>
                  <option value="Czech Republic">Czech Republic</option>
                  <option value="Denmark">Denmark</option>
                  <option value="Djibouti">Djibouti</option>
                  <option value="Dominica">Dominica</option>
                  <option value="Dominican Republic">Dominican Republic</option>
                  <option value="Ecuador">Ecuador</option>
                  <option value="Egypt">Egypt</option>
                  <option value="El Salvador">El Salvador</option>
                  <option value="Equatorial Guinea">Equatorial Guinea</option>
                  <option value="Eritrea">Eritrea</option>
                  <option value="Estonia">Estonia</option>
                  <option value="Ethiopia">Ethiopia</option>
                  <option value="Falkland Islands (Malvinas)">Falkland Islands
                    (Malvinas)
                  </option>
                  <option value="Faroe Islands">Faroe Islands</option>
                  <option value="Fiji">Fiji</option>
                  <option value="Finland">Finland</option>
                  <option value="France">France</option>
                  <option value="French Guiana">French Guiana</option>
                  <option value="French Polynesia">French Polynesia</option>
                  <option value="French Southern Territories">French Southern
                    Territories
                  </option>
                  <option value="Gabon">Gabon</option>
                  <option value="Gambia">Gambia</option>
                  <option value="Georgia">Georgia</option>
                  <option value="Germany">Germany</option>
                  <option value="Ghana">Ghana</option>
                  <option value="Gibraltar">Gibraltar</option>
                  <option value="Greece">Greece</option>
                  <option value="Greenland">Greenland</option>
                  <option value="Grenada">Grenada</option>
                  <option value="Guadeloupe">Guadeloupe</option>
                  <option value="Guam">Guam</option>
                  <option value="Guatemala">Guatemala</option>
                  <option value="Guernsey">Guernsey</option>
                  <option value="Guinea">Guinea</option>
                  <option value="Guinea-Bissau">Guinea-Bissau</option>
                  <option value="Guyana">Guyana</option>
                  <option value="Haiti">Haiti</option>
                  <option value="Heard Island and McDonald Islands">Heard Island
                    and McDonald Islands
                  </option>
                  <option value="Holy See (Vatican City State)">Holy See
                    (Vatican City State)
                  </option>
                  <option value="Honduras">Honduras</option>
                  <option value="Hong Kong">Hong Kong</option>
                  <option value="Hungary">Hungary</option>
                  <option value="Iceland">Iceland</option>
                  <option value="India">India</option>
                  <option value="Indonesia">Indonesia</option>
                  <option value="Iran, Islamic Republic of">Iran, Islamic
                    Republic of
                  </option>
                  <option value="Iraq">Iraq</option>
                  <option value="Ireland">Ireland</option>
                  <option value="Isle of Man">Isle of Man</option>
                  <option value="Israel">Israel</option>
                  <option value="Italy">Italy</option>
                  <option value="Jamaica">Jamaica</option>
                  <option value="Japan">Japan</option>
                  <option value="Jersey">Jersey</option>
                  <option value="Jordan">Jordan</option>
                  <option value="Kazakhstan">Kazakhstan</option>
                  <option value="Kenya">Kenya</option>
                  <option value="Kiribati">Kiribati</option>
                  <option value="Korea, Democratic People's Republic of">Korea,
                    Democratic People's Republic of
                  </option>
                  <option value="Korea, Republic of">Korea, Republic of</option>
                  <option value="Kuwait">Kuwait</option>
                  <option value="Kyrgyzstan">Kyrgyzstan</option>
                  <option value="Lao People's Democratic Republic">Lao People's
                    Democratic Republic
                  </option>
                  <option value="Latvia">Latvia</option>
                  <option value="Lebanon">Lebanon</option>
                  <option value="Lesotho">Lesotho</option>
                  <option value="Liberia">Liberia</option>
                  <option value="Libyan Arab Jamahiriya">Libyan Arab
                    Jamahiriya
                  </option>
                  <option value="Liechtenstein">Liechtenstein</option>
                  <option value="Lithuania">Lithuania</option>
                  <option value="Luxembourg">Luxembourg</option>
                  <option value="Macao">Macao</option>
                  <option value="Macedonia, the former Yugoslav Republic of">
                    Macedonia, the former Yugoslav Republic of
                  </option>
                  <option value="Madagascar">Madagascar</option>
                  <option value="Malawi">Malawi</option>
                  <option value="Malaysia">Malaysia</option>
                  <option value="Maldives">Maldives</option>
                  <option value="Mali">Mali</option>
                  <option value="Malta">Malta</option>
                  <option value="Marshall Islands">Marshall Islands</option>
                  <option value="Martinique">Martinique</option>
                  <option value="Mauritania">Mauritania</option>
                  <option value="Mauritius">Mauritius</option>
                  <option value="Mayotte">Mayotte</option>
                  <option value="Mexico">Mexico</option>
                  <option value="Micronesia, Federated States of">Micronesia,
                    Federated States of
                  </option>
                  <option value="Moldova, Republic of">Moldova, Republic of
                  </option>
                  <option value="Monaco">Monaco</option>
                  <option value="Mongolia">Mongolia</option>
                  <option value="Montenegro">Montenegro</option>
                  <option value="Montserrat">Montserrat</option>
                  <option value="Morocco">Morocco</option>
                  <option value="Mozambique">Mozambique</option>
                  <option value="Myanmar">Myanmar</option>
                  <option value="Namibia">Namibia</option>
                  <option value="Nauru">Nauru</option>
                  <option value="Nepal">Nepal</option>
                  <option value="Netherlands">Netherlands</option>
                  <option value="Netherlands Antilles">Netherlands Antilles
                  </option>
                  <option value="New Caledonia">New Caledonia</option>
                  <option value="New Zealand">New Zealand</option>
                  <option value="Nicaragua">Nicaragua</option>
                  <option value="Niger">Niger</option>
                  <option value="Nigeria">Nigeria</option>
                  <option value="Niue">Niue</option>
                  <option value="Norfolk Island">Norfolk Island</option>
                  <option value="Northern Mariana Islands">Northern Mariana
                    Islands
                  </option>
                  <option value="Norway">Norway</option>
                  <option value="Oman">Oman</option>
                  <option value="Pakistan">Pakistan</option>
                  <option value="Palau">Palau</option>
                  <option value="Palestine">Palestine</option>
                  <option value="Panama">Panama</option>
                  <option value="Papua New Guinea">Papua New Guinea</option>
                  <option value="Paraguay">Paraguay</option>
                  <option value="Peru">Peru</option>
                  <option value="Philippines">Philippines</option>
                  <option value="Pitcairn">Pitcairn</option>
                  <option value="Poland">Poland</option>
                  <option value="Portugal">Portugal</option>
                  <option value="Puerto Rico">Puerto Rico</option>
                  <option value="Qatar">Qatar</option>
                  <option value="Réunion">Réunion</option>
                  <option value="Romania">Romania</option>
                  <option value="Russian Federation">Russian Federation</option>
                  <option value="Rwanda">Rwanda</option>
                  <option value="Saint Barthélemy">Saint Barthélemy</option>
                  <option value="Saint Helena">Saint Helena</option>
                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis
                  </option>
                  <option value="Saint Lucia">Saint Lucia</option>
                  <option value="Saint Martin (French part)">Saint Martin
                    (French part)
                  </option>
                  <option value="Saint Pierre and Miquelon">Saint Pierre and
                    Miquelon
                  </option>
                  <option value="Saint Vincent and the Grenadines">Saint Vincent
                    and the Grenadines
                  </option>
                  <option value="Samoa">Samoa</option>
                  <option value="San Marino">San Marino</option>
                  <option value="Sao Tome and Principe">Sao Tome and Principe
                  </option>
                  <option value="Saudi Arabia">Saudi Arabia</option>
                  <option value="Senegal">Senegal</option>
                  <option value="Serbia">Serbia</option>
                  <option value="Seychelles">Seychelles</option>
                  <option value="Sierra Leone">Sierra Leone</option>
                  <option value="Singapore">Singapore</option>
                  <option value="Slovakia">Slovakia</option>
                  <option value="Slovenia">Slovenia</option>
                  <option value="Solomon Islands">Solomon Islands</option>
                  <option value="Somalia">Somalia</option>
                  <option value="South Africa">South Africa</option>
                  <option value="South Georgia and the South Sandwich Islands">
                    South Georgia and the South Sandwich Islands
                  </option>
                  <option value="South Sudan">South Sudan</option>
                  <option value="Spain">Spain</option>
                  <option value="Sri Lanka">Sri Lanka</option>
                  <option value="Sudan">Sudan</option>
                  <option value="Suriname">Suriname</option>
                  <option value="Svalbard and Jan Mayen">Svalbard and Jan
                    Mayen
                  </option>
                  <option value="Swaziland">Swaziland</option>
                  <option value="Sweden">Sweden</option>
                  <option value="Switzerland">Switzerland</option>
                  <option value="Syrian Arab Republic">Syrian Arab Republic
                  </option>
                  <option value="Taiwan, Province of China">Taiwan, Province of
                    China
                  </option>
                  <option value="Tajikistan">Tajikistan</option>
                  <option value="Tanzania, United Republic of">Tanzania, United
                    Republic of
                  </option>
                  <option value="Thailand">Thailand</option>
                  <option value="Timor-Leste">Timor-Leste</option>
                  <option value="Togo">Togo</option>
                  <option value="Tokelau">Tokelau</option>
                  <option value="Tonga">Tonga</option>
                  <option value="Trinidad and Tobago">Trinidad and Tobago
                  </option>
                  <option value="Tunisia">Tunisia</option>
                  <option value="Turkey">Turkey</option>
                  <option value="Turkmenistan">Turkmenistan</option>
                  <option value="Turks and Caicos Islands">Turks and Caicos
                    Islands
                  </option>
                  <option value="Tuvalu">Tuvalu</option>
                  <option value="Uganda">Uganda</option>
                  <option value="Ukraine">Ukraine</option>
                  <option value="United Arab Emirates">United Arab Emirates
                  </option>
                  <option value="United Kingdom">United Kingdom</option>
                  <option value="United States Minor Outlying Islands">United
                    States Minor Outlying Islands
                  </option>
                  <option value="Uruguay">Uruguay</option>
                  <option value="Uzbekistan">Uzbekistan</option>
                  <option value="Vanuatu">Vanuatu</option>
                  <option value="Venezuela, Bolivarian Republic of">Venezuela,
                    Bolivarian Republic of
                  </option>
                  <option value="Viet Nam">Viet Nam</option>
                  <option value="Virgin Islands, British">Virgin Islands,
                    British
                  </option>
                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.
                  </option>
                  <option value="Wallis and Futuna">Wallis and Futuna</option>
                  <option value="Western Sahara">Western Sahara</option>
                  <option value="Yemen">Yemen</option>
                  <option value="Zambia">Zambia</option>
                  <option value="Zimbabwe">Zimbabwe</option>
                </select></div>
            </div>
        </div>
      </div>
      <input type="hidden" name="form_id" id="form_id_2468" value="2468"/>

      <p class="um_request_name">
        <label for="um_request_2468">Only fill in if you are not human</label>
        <input type="hidden" name="um_request" id="um_request_2468" class="input" value="" size="25" autocomplete="off"/>
      </p>

      <div class="um-col-alt">


        <div class="um-center">
          <input type="submit" value="Save" class="um-button" id="um-submit-btn"/>
        </div>

    </div>

    <style>
        #um_account_submit_acc-information{
            display: none!important;
        }
    </style>

    <?php

    $output .= ob_get_contents();
    ob_end_clean();
    return $output;
}


function mg_on_register(){
    $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if(isset($_GET['otmg'])){
            $url = explode('?',$actual_link)[0];
    }else{
            $url = $actual_link;
    }

  $otmg = mg_decrypt($_GET['otmg']);
  $exp = explode('codesign', $otmg);
  $mg_full_name = $exp[1];
  $mg_email = $exp[0];

  $data = get_user_by('email',$mg_email);
  $id = $data->ID;
    programmatic_login( $data->user_login );
        //login
        wp_set_current_user($id, $data->user_login);
        wp_set_auth_cookie($id);
        do_action('wp_login', $data->user_login);

    header('Location: '.$url);
    exit;

}
add_action( 'um_registration_complete', 'mg_on_register', 10, 2 );



add_filter('wp_mail_from_name', 'new_mail_from_name');
function new_mail_from($old)
{
  return 'wordpress@codesign.com';
}
function new_mail_from_name($old)
{
  return 'The Buyability Toolkit';
}


add_action( 'wp_ajax_save_page_checkboxes', 'save_page_checkboxes' );
add_action( 'wp_ajax_nopriv_save_page_checkboxes', 'save_page_checkboxes' );
function save_page_checkboxes() {
  global $current_user;
  if ( isset( $_POST['page'] ) && isset( $_POST['data'] ) ) {
    if(is_user_logged_in()){
      update_user_meta($current_user->ID,$_POST['page'].'_checkboxes',$_POST['data']);
      //echo 'saved';
    }
  }
  exit;
}

//save_page_selects
add_action( 'wp_ajax_save_page_inputs', 'save_page_inputs' );
add_action( 'wp_ajax_nopriv_save_page_inputs', 'save_page_inputs' );
function save_page_inputs() {
  global $current_user;
  if ( isset( $_POST['page'] ) && isset( $_POST['data'] ) ) {
    if(is_user_logged_in()){
      update_user_meta($current_user->ID,$_POST['page'],$_POST['data']);
      //echo 'saved';
    }
  }
  exit;
}

add_action( 'wp_ajax_save_page_selects', 'save_page_selects' );
add_action( 'wp_ajax_nopriv_save_page_selects', 'save_page_selects' );
function save_page_selects() {
  global $current_user;
  if ( isset( $_POST['page'] ) && isset( $_POST['data'] ) ) {
    if(is_user_logged_in()){
      update_user_meta($current_user->ID,$_POST['page'].'_selects',$_POST['data']);
      //echo 'saved';
    }
  }
  exit;
}

add_action( 'wp_ajax_get_page_inputs', 'get_page_inputs' );
add_action( 'wp_ajax_nopriv_get_page_inputs', 'get_page_inputs' );
function get_page_inputs() {
  global $current_user;
  if ( isset( $_POST['page'] ) ) {
    if(is_user_logged_in()){
     echo get_user_meta($current_user->ID,$_POST['page'],true);

    }
  }
  exit;
}

add_action( 'wp_ajax_get_page_selects', 'get_page_selects' );
add_action( 'wp_ajax_nopriv_get_page_selects', 'get_page_selects' );
function get_page_selects() {
  global $current_user;
  if ( isset( $_POST['page'] ) ) {
    if(is_user_logged_in()){
     echo get_user_meta($current_user->ID,$_POST['page'].'_selects',true);

    }
  }
  exit;
}

add_action( 'wp_ajax_get_page_checkboxes', 'get_page_checkboxes' );
add_action( 'wp_ajax_nopriv_get_page_checkboxes', 'get_page_checkboxes' );
function get_page_checkboxes() {
  global $current_user;
  if ( isset( $_POST['page'] ) ) {
    if(is_user_logged_in()){
     echo get_user_meta($current_user->ID,$_POST['page'].'_checkboxes',true);

    }
  }
  exit;
}

function buy_store_values_trigger(){
global $post;
$array = array('magicnumbercalculator','valueandgrowthcalculator','ideallife','ideal-life');

if(in_array($post->post_name,$array)):
?>

<script>
  //Onkeyup any input
  //Get all inputs
  //Update by page for user

function clear_values(){

  jQuery('input').each(function(index, value) {
  var val = jQuery(value).val('')
});

  jQuery('select').each(function(index, value) {
  var val = jQuery(value).val('')
});

var page = '<?= $post->post_name; ?>';


  var array = {};

jQuery('select').each(function(index, value) {
  ////console.log(value);
  var val = jQuery(value).val()
  ////console.log(index + ' | ' +val);
    array[index] = val;


});

save_select_for_page(page,array);


  var array = {};

  jQuery('input').each(function(index, value) {
  ////console.log(value);
  var val = jQuery(value).val()
  ////console.log(index + ' | ' +val);
    array[index] = val;


});

save_input_for_page(page,array);



}

jQuery( document ).ready(function() {
   get_input_for_page('<?= $post->post_name; ?>');
   get_select_for_page('<?= $post->post_name; ?>');
   get_checkboxes_for_page('<?= $post->post_name; ?>');
   jQuery('.question .buttons').append('<a style="margin-left:8px;" onclick="clear_values();" class="btn btn-medium" href="#">Clear Values</a>');

});


function save_checkboxes_for_page(page,array){
  var url = '<?= admin_url( 'admin-ajax.php' ); ?>';
 jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
          page: page,
          data: JSON.stringify(array),
          action: 'save_page_checkboxes',
        },
        success: function (data, status) {
        },
        error: function (xhr, desc, err) {
        }
      });
}

function save_select_for_page(page,array){

var url = '<?= admin_url( 'admin-ajax.php' ); ?>';
 jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
          page: page,
          data: JSON.stringify(array),
          action: 'save_page_selects',
        },
        success: function (data, status) {
        },
        error: function (xhr, desc, err) {
        }
      });
}

function save_input_for_page(page,array){

var url = '<?= admin_url( 'admin-ajax.php' ); ?>';
 jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
          page: page,
          data: JSON.stringify(array),
          action: 'save_page_inputs',
        },
        success: function (data, status) {
        },
        error: function (xhr, desc, err) {
        }
      });
}

function get_select_for_page(page){

var url = '<?= admin_url( 'admin-ajax.php' ); ?>';
 jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
          page: page,
          action: 'get_page_selects',
        },
        success: function (data, status) {
          var selects = JSON.parse(data);
            jQuery('select').each(function(index, value) {
    //console.log(selects[index]);
     jQuery(value).val(selects[index]);


});
        },
        error: function (xhr, desc, err) {
        }
      });
}


function get_input_for_page(page){

var url = '<?= admin_url( 'admin-ajax.php' ); ?>';
 jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
          page: page,
          action: 'get_page_inputs',
        },
        success: function (data, status) {
          var inputs = JSON.parse(data);
            jQuery('input').each(function(index, value) {
    //console.log(inputs[index]);
     jQuery(value).val(inputs[index]);


});
        },
        error: function (xhr, desc, err) {
        }
      });
}

function get_checkboxes_for_page(page){

var url = '<?= admin_url( 'admin-ajax.php' ); ?>';
 jQuery.ajax({
        url: url,
        type: 'POST',
        data: {
          page: page,
          action: 'get_page_checkboxes',
        },
        success: function (data, status) {
          var checkboxess = JSON.parse(data);
            jQuery('input[type="checkbox"]').each(function(index, value) {
    //console.log(checkboxess[index]);
     
     if(checkboxess[index] == 1){
      var ind = index + 1;
      //unblock_this(value,'x' + ind);
      jQuery('.x' + ind).removeAttr('disabled');
  jQuery(value).prop('checked', true);
     }else{
  jQuery(value).prop('checked', false);
     }


});
        },
        error: function (xhr, desc, err) {
        }
      });
}


var page = '<?= $post->post_name; ?>';

jQuery('input').keyup(function (data) {
  var array = {};

  jQuery('input').each(function(index, value) {
  ////console.log(value);
  var val = jQuery(value).val()
  ////console.log(index + ' | ' +val);
    array[index] = val;


});

save_input_for_page(page,array);


});


jQuery('select').change(function (data) {
  var array = {};

  jQuery('select').each(function(index, value) {
  ////console.log(value);
  var val = jQuery(value).val()
  ////console.log(index + ' | ' +val);
    array[index] = val;


});

save_select_for_page(page,array);


});

//

jQuery('input[type="checkbox"]').click(function (data) {
  var array = {};

  jQuery('input[type="checkbox"]').each(function(index, value) {
  ////console.log(value);
  var val = jQuery(value).is(':checked');
  if(val === true){
    var vx = 1;
  }else{
    var vx = 0;
  }
  ////console.log(index + ' | ' +val);
    array[index] = vx;


});
//console.log(array);
save_checkboxes_for_page(page,array);

  var array = {};

  jQuery('input').each(function(index, value) {
  ////console.log(value);
  var val = jQuery(value).val()
  ////console.log(index + ' | ' +val);
    array[index] = val;


});

save_input_for_page(page,array);

});
</script>

<?php
endif;
}
add_action('wp_footer','buy_store_values_trigger');