<div style="background: #f2f2f2;"><div style="max-width: 760px;padding: 20px;background: #ffffff;border-radius: 5px;margin:40px auto;font-family: Open Sans,Helvetica,Arial;font-size: 15px;color: #666;">

    <div style="color: #444444;font-weight: normal;">
        <div style="text-align: center;font-weight:600;font-size:26px;padding: 10px 0;border-bottom: solid 3px #eeeeee;">
            <?= get_bloginfo( 'name' ); ?></div>

        <div style="clear:both"></div>
    </div>

    <div style="padding: 0 30px 30px 30px;border-bottom: 3px solid #eeeeee;">

        <div style="padding: 30px 0;font-size: 24px;text-align: center;line-height: 40px;">Verify your email to finish signing up!</div>

        <div style="padding: 0 0 15px 0;">

            <div style="background: #eee;color: #444;padding: 12px 15px; border-radius: 3px;font-weight: bold;font-size: 16px;">Action needed: <div style="padding: 10px 15px 0 15px;color: #333;"><span style="color:#999">
        <p>Please confirm that <strong><a target="_blank" href="mailto:<?= $mg_email; ?>" style="text-decoration: none;"><?= $mg_email; ?></a></strong>&nbsp;is your email address by clicking on the button below or use this link <a target="_blank" href="<?= $mg_targeturl . "?otmg=" . $otmg;?>" style="text-decoration: none; word-break: break-all;"><?= $mg_targeturl . "?otmg=" . $otmg;?></a> within <strong>48 hours</strong>
        </span> <span style="font-weight:bold"></span></div></div>



        </div>

    </div>

    <div style="color: #999;padding: 20px 30px">

        <div style="">Thank you!</div>
        <div style=""><a href="<?= site_url(); ?>" style="color: #3ba1da;text-decoration: none;"> <?= get_bloginfo( 'name' ); ?></a> Team</div>

    </div>

</div>
</div>
