<?php

function mg_display_capturer()
{

  $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  if (is_user_logged_in()) {
    if (strpos($actual_link, '/buyabilityassessment/') !== false) {
      ?>
      <script>
        function mg_action() {
          const apiLink = 'https://buyablebusiness.com/wp-json/wp/v2/questions?per_page=100&_embed';
          let isLoading = true;
          const categories = {};
          const categoriesArray = [];
          const fetchedQuestions = fetch(apiLink)
            .then(res => res.json())
            .then(json => {
              isLoading = false;
              window.dispatchEvent(new Event('questionsLoaded'));

              json.map((question, index) => {
                const questionObj = {
                  id: question.id,
                  category: question.questions_categories[0],
                  title: question.title.rendered,
                  suggested_threshold: +question.acf.suggestion_threshold,
                  suggestion_message: question.acf.suggestion_button,
                  result: 0,
                };
                categories[question.questions_categories[0]] ?
                  categories[question.questions_categories[0]].questions.push(questionObj) :
                  (categories[question.questions_categories[0]] = {
                    name: question._embedded['wp:term'][0][0].name,
                    color: question._embedded['wp:term'][0][0].acf.ba_color,
                    suggested_threshold: +question._embedded['wp:term'][0][0].acf.ba_suggested_threshold,
                    suggestion_link: question._embedded['wp:term'][0][0].acf.ba_suggestion_message,
                    suggestion_message: question._embedded['wp:term'][0][0].acf.ba_suggestion_message_top,
                    questions: [questionObj],
                    result: 0,
                  });
              });

              console.log(categories);
              categoriesArray.push(...Object.values(categories))
            });

          step1.classList.remove('active');
          step1.nextElementSibling.classList.add('active');
          fetchedQuestions.then(() => {
            loading.classList.add('active');
            currentCategory = categoriesArray[currentCategoryIndex];

            let maxHeight = 0;
            $question.style.height = 'auto';
            nextQues.classList.add('active');
            for (let i = 0; i < currentCategory.questions.length; i++) {
              setNextQuestion(currentCategory, i)
              const {height} = $question.getBoundingClientRect();
              height > maxHeight && (maxHeight = height);
            }
            $question.style.height = maxHeight / parseFloat(document.documentElement.style.fontSize) + 'rem';
            nextQues.classList.remove('active');

            setNextQuestion(currentCategory, currentQuestionIndex)
          });

        }
      </script>
      <?php
    } else {
      ?>
      <script>
        function mg_action() {
          const step1 = document.querySelector('#step-1');
          step1.classList.remove('active');
          step1.nextElementSibling.classList.add('active');
          jQuery('html, body').animate({scrollTop: 0}, 'slow');
        }
      </script>
      <?php
    }
  } else {
    ?>
    <script>
      function mg_action() {
        //alert('Login First');

        Swal.fire({
          icon: 'success',
          title: 'Thank you! Please confirm your email to continue.',
          text: 'We have just sent you an email with a confirmation link, Please check your inbox!',
          showConfirmButton: false,
          timer: 4500
        });

        jQuery('.swal2-input').css('display', 'none!important');
        jQuery('.swal2-file').css('display', 'none!important');
      }
    </script>
    <style>
      .swal2-show {
        display: grid;
        width: 50%;
        font-size: 16px;
      }

      .swal2-input, .swal2-file {
        display: none !important;
      }
    </style>
    <?php
  }

  if (!is_user_logged_in()) {
    ?>
    <style>
      #mg_container h2 {
        background: url(https://static.pexels.com/photos/5782/nature-flowers-vintage-plant.jpg);
        -webkit-text-fill-color: transparent;
        -webkit-background-clip: text;
        /* 	background-image: linear-gradient(to top, #cd9cf2 0%, #f6f3ff 100%); */
        font-family: "Playfair Display", serif;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        letter-spacing: 2px;
        font-size: 3.5em;
        margin: 0;

      }

      #mg_container p {
        font-family: "Farsan", cursive;
        margin: 3px 0 1.5em 0;
        font-size: 1.3em;
        color: #7d7d7d;
      }

      /*#mg_container input {*/
      /*  !* 	background: rgba(255, 13, 254, 0.90); *!*/
      /*  width: 210px;*/
      /*  display: inline-block;*/
      /*  text-align: center;*/
      /*  border-radius: 7px;*/
      /*  background: #eee;*/
      /*  padding: 1em 2em;*/
      /*  outline: none;*/
      /*  border: none;*/
      /*  color: #222;*/
      /*  transition: 0.3s linear;*/
      /*}*/

      /*::placeholder {*/
      /*  color: #999;*/
      /*}*/

      /*#mg_container input:focus {*/
      /*  background: rgba(0, 0, 333, 0.1);*/
      /*}*/


      #mg_container button:hover {
        transform: translatey(2px);
      }

      #mg_container button:active {
        transform: translatey(5px);
      }

    </style>

    <script>


      function isEmail(email) {
        var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return regex.test(email);
      }

      function mg_submit() {
        var url = '<?= admin_url('admin-ajax.php?action=mg_receive_user_data'); ?>';
        var mg_email = jQuery('#mg_email').val();
        var mg_full_name = jQuery('#mg_full_name').val();
        var targeturl = jQuery('#targeturl').val();

        if (mg_email != '' && mg_full_name != '' && isEmail(mg_email)) {
          jQuery.ajax({
            type: "POST",
            url: url,
            data: {
              'mg_full_name': mg_full_name,
              'mg_email': mg_email,
              'targeturl': targeturl
            }, // serializes the form's elements.
            success: function (data) {
              if (data == 'found') {
                jQuery('#err').remove();
                var err = '<span class="form-error-message" style="color:red;font-size:15px;" id="err">You have already registered with us, <a href="<?= site_url() . '/login/'; ?>">Click here to login</a></span>';
                jQuery('#mg_container').append(err);
              } else {
                mg_action();
              }

            }
          });
        } else {
          if (mg_email == '' || !isEmail(mg_email)) {
            jQuery('#mg_email').css('border', '1px solid red');
          }
          if (mg_full_name == '') {
            jQuery('#mg_full_name').css('border', '1px solid red');
          }

        }


      }

      jQuery(document).ready(function () {
        jQuery('.begin-btn').attr('onClick', 'mg_submit();');
      });
    </script>

    <style>

      @media only screen and (min-width: 40em) {
        .modal-overlay {
          display: flex;
          align-items: center;
          justify-content: center;
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          z-index: 5;
          background-color: rgba(0, 0, 0, 0.6);
          opacity: 0;
          visibility: hidden;
          -webkit-backface-visibility: hidden;
          backface-visibility: hidden;
          transition: opacity 0.6s cubic-bezier(0.55, 0, 0.1, 1), visibility 0.6s cubic-bezier(0.55, 0, 0.1, 1);
        }

        .modal-overlay.active {
          opacity: 1;
          visibility: visible;
        }
      }

      /**
       * Modal
       */
      .modal {
        display: flex;
        align-items: center;
        justify-content: center;
        position: relative;
        margin: 0 auto;
        background-color: #fff;
        width: 600px;
        max-width: 75rem;
        min-height: 20rem;
        padding: 1rem;
        border-radius: 3px;
        opacity: 0;
        overflow-y: auto;
        visibility: hidden;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        transform: scale(1.2);
        transition: all 0.6s cubic-bezier(0.55, 0, 0.1, 1);
      }

      .modal .close-modal {
        position: absolute;
        cursor: pointer;
        top: 5px;
        right: 15px;
        opacity: 0;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        transition: opacity 0.6s cubic-bezier(0.55, 0, 0.1, 1), transform 0.6s cubic-bezier(0.55, 0, 0.1, 1);
        transition-delay: 0.3s;
      }

      .modal .close-modal svg {
        width: 1.75em;
        height: 1.75em;
      }

      .modal .modal-content {
        opacity: 0;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        transition: opacity 0.6s cubic-bezier(0.55, 0, 0.1, 1);
        transition-delay: 0.3s;
      }

      .modal.active {
        visibility: visible;
        opacity: 1;
        transform: scale(1);
      }

      .modal.active .modal-content {
        opacity: 1;
      }

      .modal.active .close-modal {
        transform: translateY(10px);
        opacity: 1;
      }

      /**
       * Mobile styling
       */
      @media only screen and (max-width: 39.9375em) {
        h1 {
          font-size: 1.5rem;
        }

        .modal {
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          -webkit-overflow-scrolling: touch;
          border-radius: 0;
          transform: scale(1.1);
          padding: 0 !important;
        }

        .close-modal {
          right: 20px !important;
        }
      }
    </style>

    <?php
    if (!is_user_logged_in() && isset($_GET['otmg']) && !isset($_GET['um_form_id'])):
      ?>
      <script>
        jQuery(document).ready(function () {
          var elements = jQuery(".modal-overlay, .modal");
          elements.addClass("active");
        });
      </script>
      <div class="modal-overlay">
        <div class="modal">
          <!--
              <a class="close-modal">
                <svg viewBox="0 0 20 20">
                  <path fill="#000000" d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path>
                </svg>
              </a>-->

          <div class="modal-content">
            <h3><?= mg_confirmation_form(); ?></h3>
          </div><!-- content -->

        </div><!-- modal -->
      </div><!-- overlay -->
    <?php
    endif;
    ?>
    <!-- modal -->

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <div id="mg_container" class="inputs-wrapper">
      <input id="mg_full_name" placeholder="Type Your First Name" required>
      <input type="hidden" id="targeturl" value="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
      <input class="" id="mg_email" type="email" placeholder="Type Your Email" required>
    </div>

    <?php
  } else {
    ?>
    <script>

      function mg_submit() {
        const step1 = document.querySelector('#step-1');
        step1.classList.remove('active');
        step1.nextElementSibling.classList.add('active');
        jQuery('html, body').animate({scrollTop: 0}, 'slow');
      }

      jQuery(document).ready(function () {
        jQuery('.begin-btn').attr('onClick', 'mg_submit();');
      });
    </script>
    <?php
  }
}

add_shortcode('mg_display_capturer', 'mg_display_capturer');


function mg_confirmation_form()
{

  $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

  if (!isset($_GET['otmg'])) {
    header('Location: ' . site_url());
    exit;
  }

  ob_start();

// 	if(isset($_POST['first_name-2468'])){
// 		$email = $_POST['user_email-2468'];
// 		$username = explode('@',$_POST['user_email-2468'])[0];
// 		$password = $_POST['user_password-2468'];
// 		$name = $_POST['first_name-2468'];
// 		$user_id = wp_create_user( $username, $password, $email );
// 		if(is_wp_error($user_id)){
//   		$error = $user_id->get_error_message();
//   		echo 'Error!';
//   //handle error here
// }else{
//         wp_clear_auth_cookie();
//     	wp_set_current_user ( $user_id );
//     	wp_set_auth_cookie  ( $user_id );
//     	update_user_meta($user_id,'first_name',$name);
//     	do_action('wp_login', $username);
// 		echo 'Success!';
//   //handle successful creation here
// }

// 	}

  $otmg = mg_decrypt($_GET['otmg']);
  $exp = explode('codesign', $otmg);
  $mg_full_name = $exp[1];
  $mg_email = $exp[0];

  ?>

  <script>
    jQuery(document).ready(function () {
      jQuery('#first_name-1336').attr('readonly', true);
      jQuery('#user_email-1336').attr('readonly', true);
      jQuery('#first_name-1336').val('<?= $mg_full_name; ?>');
      jQuery('#user_email-1336').val('<?= $mg_email; ?>');
      jQuery('.page-transition').find('.uimob500').insertAfter('#block_6110f41ee797a');
    });
  </script>

  <?php
  echo do_shortcode('[ultimatemember form_id="1336"]');

  $content = ob_get_clean();
  return $content;
}

add_shortcode('mg_confirmation_form_x', 'mg_confirmation_form');


function mg_upgrade()
{
  global $current_user;
  //var_dump($_POST);
  if (isset($_POST['first_name-2468'])) {
    foreach ($_POST as $key => $val) {
      if (strpos($key, '-2468') !== false) {
        $meta_key = explode('-2468', $key)[0];
        $meta_value = $val;

        update_user_meta($current_user->ID, $meta_key, $meta_value);

      }
    }

    $u = new WP_User( $current_user->ID );

// Remove role
$u->remove_role( 'subscriber' );

// Add role
$u->add_role( 'buyable_upgraded' );

header('Location: '.site_url().'/?upgrade=1');
exit;
  }

  ob_start();
  ?>
  <div id="um-admin-form-shortcode" class="postbox ">
  <div class="inside">
    <div class="um-admin-metabox">
      <div class="um um-register um-2468">

        <div class="um-form" data-mode="register">

          <form method="post">

            <div id="um_field_2468_first_name" class="um-field um-field-text  um-field-first_name um-field-text um-field-type_text" data-key="first_name">
              <div class="um-field-label"><label for="first_name-2468">Full
                  Name</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid " type="text" name="first_name-2468" id="first_name-2468" value="<?= get_user_meta($current_user->ID, 'first_name', true); ?>" placeholder="" data-validate="" data-key="first_name"/>

              </div>
            </div>
            <div id="um_field_2468_phone_number" class="um-field um-field-text  um-field-phone_number um-field-text um-field-type_text" data-key="phone_number">
              <div class="um-field-label">
                <div class="um-field-label-icon">
                  <i class="um-faicon-phone" aria-label="Phone Number"></i>
                </div>
                <label for="phone_number-2468">Phone Number</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid " type="text" name="phone_number-2468" id="phone_number-2468" value="<?= get_user_meta($current_user->ID, 'phone_number', true); ?>" placeholder="" data-validate="phone_number" data-key="phone_number"/>

              </div>
            </div>
            <div id="um_field_2468_address" class="um-field um-field-text  um-field-address um-field-text um-field-type_text" data-key="address">
              <div class="um-field-label">
                <label for="address-2468">Address</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="address-2468" id="address-2468" value="<?= get_user_meta($current_user->ID, 'address', true); ?>" placeholder="Address" data-validate="" data-key="address"/>

              </div>
            </div>
            <div id="um_field_2468_city" class="um-field um-field-text  um-field-city um-field-text um-field-type_text" data-key="city">
              <div class="um-field-label"><label for="city-2468">city</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="city-2468" id="city-2468" value="<?= get_user_meta($current_user->ID, 'city', true); ?>" placeholder="city" data-validate="" data-key="city"/>

              </div>
            </div>
            <div id="um_field_2468_Zip_Code" class="um-field um-field-text  um-field-Zip_Code um-field-text um-field-type_text" data-key="Zip_Code">
              <div class="um-field-label"><label for="Zip_Code-2468">Zip
                  Code</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="Zip_Code-2468" id="Zip_Code-2468" value="<?= get_user_meta($current_user->ID, 'Zip_Code', true); ?>" placeholder="Zip Code" data-validate="" data-key="Zip_Code"/>

              </div>
            </div>
            <div id="um_field_2468_State" class="um-field um-field-text  um-field-State um-field-text um-field-type_text" data-key="State">
              <div class="um-field-label"><label for="State-2468">State</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area">
                <input autocomplete="off" class="um-form-field valid not-required " type="text" name="State-2468" id="State-2468" value="<?= get_user_meta($current_user->ID, 'State', true); ?>" placeholder="State" data-validate="" data-key="State"/>

              </div>
            </div>
            <div id="um_field_2468_country" class="um-field um-field-select  um-field-country um-field-select um-field-type_select" data-key="country">
              <div class="um-field-label">
                <label for="country-2468">Country</label>
                <div class="um-clear"></div>
              </div>
              <div class="um-field-area  ">
                <select data-default="" name="country-2468" id="country" data-validate="" data-key="country" class="um-form-field valid um-s1 " style="width: 100%" data-placeholder="Choose a Country">
                  <option value=""></option>
                  <?php
                  if (get_user_meta($current_user->ID, 'country', true) != '') {
                    ?>
                    <option value="<?= get_user_meta($current_user->ID, 'country', true); ?>" selected><?= get_user_meta($current_user->ID, 'country', true); ?></option>
                    <?php
                  }
                  ?>
                  <option value="United States">United States</option>
                  <option value="Afghanistan">Afghanistan</option>
                  <option value="Åland Islands">Åland Islands</option>
                  <option value="Albania">Albania</option>
                  <option value="Algeria">Algeria</option>
                  <option value="American Samoa">American Samoa</option>
                  <option value="Andorra">Andorra</option>
                  <option value="Angola">Angola</option>
                  <option value="Anguilla">Anguilla</option>
                  <option value="Antarctica">Antarctica</option>
                  <option value="Antigua and Barbuda">Antigua and Barbuda
                  </option>
                  <option value="Argentina">Argentina</option>
                  <option value="Armenia">Armenia</option>
                  <option value="Aruba">Aruba</option>
                  <option value="Australia">Australia</option>
                  <option value="Austria">Austria</option>
                  <option value="Azerbaijan">Azerbaijan</option>
                  <option value="Bahamas">Bahamas</option>
                  <option value="Bahrain">Bahrain</option>
                  <option value="Bangladesh">Bangladesh</option>
                  <option value="Barbados">Barbados</option>
                  <option value="Belarus">Belarus</option>
                  <option value="Belgium">Belgium</option>
                  <option value="Belize">Belize</option>
                  <option value="Benin">Benin</option>
                  <option value="Bermuda">Bermuda</option>
                  <option value="Bhutan">Bhutan</option>
                  <option value="Bolivia, Plurinational State of">Bolivia,
                    Plurinational State of
                  </option>
                  <option value="Bosnia and Herzegovina">Bosnia and
                    Herzegovina
                  </option>
                  <option value="Botswana">Botswana</option>
                  <option value="Bouvet Island">Bouvet Island</option>
                  <option value="Brazil">Brazil</option>
                  <option value="British Indian Ocean Territory">British Indian
                    Ocean Territory
                  </option>
                  <option value="Brunei Darussalam">Brunei Darussalam</option>
                  <option value="Bulgaria">Bulgaria</option>
                  <option value="Burkina Faso">Burkina Faso</option>
                  <option value="Burundi">Burundi</option>
                  <option value="Cambodia">Cambodia</option>
                  <option value="Cameroon">Cameroon</option>
                  <option value="Canada">Canada</option>
                  <option value="Cape Verde">Cape Verde</option>
                  <option value="Cayman Islands">Cayman Islands</option>
                  <option value="Central African Republic">Central African
                    Republic
                  </option>
                  <option value="Chad">Chad</option>
                  <option value="Chile">Chile</option>
                  <option value="China">China</option>
                  <option value="Christmas Island">Christmas Island</option>
                  <option value="Cocos (Keeling) Islands">Cocos (Keeling)
                    Islands
                  </option>
                  <option value="Colombia">Colombia</option>
                  <option value="Comoros">Comoros</option>
                  <option value="Congo">Congo</option>
                  <option value="Congo, the Democratic Republic of the">Congo,
                    the Democratic Republic of the
                  </option>
                  <option value="Cook Islands">Cook Islands</option>
                  <option value="Costa Rica">Costa Rica</option>
                  <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                  <option value="Croatia">Croatia</option>
                  <option value="Cuba">Cuba</option>
                  <option value="Cyprus">Cyprus</option>
                  <option value="Czech Republic">Czech Republic</option>
                  <option value="Denmark">Denmark</option>
                  <option value="Djibouti">Djibouti</option>
                  <option value="Dominica">Dominica</option>
                  <option value="Dominican Republic">Dominican Republic</option>
                  <option value="Ecuador">Ecuador</option>
                  <option value="Egypt">Egypt</option>
                  <option value="El Salvador">El Salvador</option>
                  <option value="Equatorial Guinea">Equatorial Guinea</option>
                  <option value="Eritrea">Eritrea</option>
                  <option value="Estonia">Estonia</option>
                  <option value="Ethiopia">Ethiopia</option>
                  <option value="Falkland Islands (Malvinas)">Falkland Islands
                    (Malvinas)
                  </option>
                  <option value="Faroe Islands">Faroe Islands</option>
                  <option value="Fiji">Fiji</option>
                  <option value="Finland">Finland</option>
                  <option value="France">France</option>
                  <option value="French Guiana">French Guiana</option>
                  <option value="French Polynesia">French Polynesia</option>
                  <option value="French Southern Territories">French Southern
                    Territories
                  </option>
                  <option value="Gabon">Gabon</option>
                  <option value="Gambia">Gambia</option>
                  <option value="Georgia">Georgia</option>
                  <option value="Germany">Germany</option>
                  <option value="Ghana">Ghana</option>
                  <option value="Gibraltar">Gibraltar</option>
                  <option value="Greece">Greece</option>
                  <option value="Greenland">Greenland</option>
                  <option value="Grenada">Grenada</option>
                  <option value="Guadeloupe">Guadeloupe</option>
                  <option value="Guam">Guam</option>
                  <option value="Guatemala">Guatemala</option>
                  <option value="Guernsey">Guernsey</option>
                  <option value="Guinea">Guinea</option>
                  <option value="Guinea-Bissau">Guinea-Bissau</option>
                  <option value="Guyana">Guyana</option>
                  <option value="Haiti">Haiti</option>
                  <option value="Heard Island and McDonald Islands">Heard Island
                    and McDonald Islands
                  </option>
                  <option value="Holy See (Vatican City State)">Holy See
                    (Vatican City State)
                  </option>
                  <option value="Honduras">Honduras</option>
                  <option value="Hong Kong">Hong Kong</option>
                  <option value="Hungary">Hungary</option>
                  <option value="Iceland">Iceland</option>
                  <option value="India">India</option>
                  <option value="Indonesia">Indonesia</option>
                  <option value="Iran, Islamic Republic of">Iran, Islamic
                    Republic of
                  </option>
                  <option value="Iraq">Iraq</option>
                  <option value="Ireland">Ireland</option>
                  <option value="Isle of Man">Isle of Man</option>
                  <option value="Israel">Israel</option>
                  <option value="Italy">Italy</option>
                  <option value="Jamaica">Jamaica</option>
                  <option value="Japan">Japan</option>
                  <option value="Jersey">Jersey</option>
                  <option value="Jordan">Jordan</option>
                  <option value="Kazakhstan">Kazakhstan</option>
                  <option value="Kenya">Kenya</option>
                  <option value="Kiribati">Kiribati</option>
                  <option value="Korea, Democratic People's Republic of">Korea,
                    Democratic People's Republic of
                  </option>
                  <option value="Korea, Republic of">Korea, Republic of</option>
                  <option value="Kuwait">Kuwait</option>
                  <option value="Kyrgyzstan">Kyrgyzstan</option>
                  <option value="Lao People's Democratic Republic">Lao People's
                    Democratic Republic
                  </option>
                  <option value="Latvia">Latvia</option>
                  <option value="Lebanon">Lebanon</option>
                  <option value="Lesotho">Lesotho</option>
                  <option value="Liberia">Liberia</option>
                  <option value="Libyan Arab Jamahiriya">Libyan Arab
                    Jamahiriya
                  </option>
                  <option value="Liechtenstein">Liechtenstein</option>
                  <option value="Lithuania">Lithuania</option>
                  <option value="Luxembourg">Luxembourg</option>
                  <option value="Macao">Macao</option>
                  <option value="Macedonia, the former Yugoslav Republic of">
                    Macedonia, the former Yugoslav Republic of
                  </option>
                  <option value="Madagascar">Madagascar</option>
                  <option value="Malawi">Malawi</option>
                  <option value="Malaysia">Malaysia</option>
                  <option value="Maldives">Maldives</option>
                  <option value="Mali">Mali</option>
                  <option value="Malta">Malta</option>
                  <option value="Marshall Islands">Marshall Islands</option>
                  <option value="Martinique">Martinique</option>
                  <option value="Mauritania">Mauritania</option>
                  <option value="Mauritius">Mauritius</option>
                  <option value="Mayotte">Mayotte</option>
                  <option value="Mexico">Mexico</option>
                  <option value="Micronesia, Federated States of">Micronesia,
                    Federated States of
                  </option>
                  <option value="Moldova, Republic of">Moldova, Republic of
                  </option>
                  <option value="Monaco">Monaco</option>
                  <option value="Mongolia">Mongolia</option>
                  <option value="Montenegro">Montenegro</option>
                  <option value="Montserrat">Montserrat</option>
                  <option value="Morocco">Morocco</option>
                  <option value="Mozambique">Mozambique</option>
                  <option value="Myanmar">Myanmar</option>
                  <option value="Namibia">Namibia</option>
                  <option value="Nauru">Nauru</option>
                  <option value="Nepal">Nepal</option>
                  <option value="Netherlands">Netherlands</option>
                  <option value="Netherlands Antilles">Netherlands Antilles
                  </option>
                  <option value="New Caledonia">New Caledonia</option>
                  <option value="New Zealand">New Zealand</option>
                  <option value="Nicaragua">Nicaragua</option>
                  <option value="Niger">Niger</option>
                  <option value="Nigeria">Nigeria</option>
                  <option value="Niue">Niue</option>
                  <option value="Norfolk Island">Norfolk Island</option>
                  <option value="Northern Mariana Islands">Northern Mariana
                    Islands
                  </option>
                  <option value="Norway">Norway</option>
                  <option value="Oman">Oman</option>
                  <option value="Pakistan">Pakistan</option>
                  <option value="Palau">Palau</option>
                  <option value="Palestine">Palestine</option>
                  <option value="Panama">Panama</option>
                  <option value="Papua New Guinea">Papua New Guinea</option>
                  <option value="Paraguay">Paraguay</option>
                  <option value="Peru">Peru</option>
                  <option value="Philippines">Philippines</option>
                  <option value="Pitcairn">Pitcairn</option>
                  <option value="Poland">Poland</option>
                  <option value="Portugal">Portugal</option>
                  <option value="Puerto Rico">Puerto Rico</option>
                  <option value="Qatar">Qatar</option>
                  <option value="Réunion">Réunion</option>
                  <option value="Romania">Romania</option>
                  <option value="Russian Federation">Russian Federation</option>
                  <option value="Rwanda">Rwanda</option>
                  <option value="Saint Barthélemy">Saint Barthélemy</option>
                  <option value="Saint Helena">Saint Helena</option>
                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis
                  </option>
                  <option value="Saint Lucia">Saint Lucia</option>
                  <option value="Saint Martin (French part)">Saint Martin
                    (French part)
                  </option>
                  <option value="Saint Pierre and Miquelon">Saint Pierre and
                    Miquelon
                  </option>
                  <option value="Saint Vincent and the Grenadines">Saint Vincent
                    and the Grenadines
                  </option>
                  <option value="Samoa">Samoa</option>
                  <option value="San Marino">San Marino</option>
                  <option value="Sao Tome and Principe">Sao Tome and Principe
                  </option>
                  <option value="Saudi Arabia">Saudi Arabia</option>
                  <option value="Senegal">Senegal</option>
                  <option value="Serbia">Serbia</option>
                  <option value="Seychelles">Seychelles</option>
                  <option value="Sierra Leone">Sierra Leone</option>
                  <option value="Singapore">Singapore</option>
                  <option value="Slovakia">Slovakia</option>
                  <option value="Slovenia">Slovenia</option>
                  <option value="Solomon Islands">Solomon Islands</option>
                  <option value="Somalia">Somalia</option>
                  <option value="South Africa">South Africa</option>
                  <option value="South Georgia and the South Sandwich Islands">
                    South Georgia and the South Sandwich Islands
                  </option>
                  <option value="South Sudan">South Sudan</option>
                  <option value="Spain">Spain</option>
                  <option value="Sri Lanka">Sri Lanka</option>
                  <option value="Sudan">Sudan</option>
                  <option value="Suriname">Suriname</option>
                  <option value="Svalbard and Jan Mayen">Svalbard and Jan
                    Mayen
                  </option>
                  <option value="Swaziland">Swaziland</option>
                  <option value="Sweden">Sweden</option>
                  <option value="Switzerland">Switzerland</option>
                  <option value="Syrian Arab Republic">Syrian Arab Republic
                  </option>
                  <option value="Taiwan, Province of China">Taiwan, Province of
                    China
                  </option>
                  <option value="Tajikistan">Tajikistan</option>
                  <option value="Tanzania, United Republic of">Tanzania, United
                    Republic of
                  </option>
                  <option value="Thailand">Thailand</option>
                  <option value="Timor-Leste">Timor-Leste</option>
                  <option value="Togo">Togo</option>
                  <option value="Tokelau">Tokelau</option>
                  <option value="Tonga">Tonga</option>
                  <option value="Trinidad and Tobago">Trinidad and Tobago
                  </option>
                  <option value="Tunisia">Tunisia</option>
                  <option value="Turkey">Turkey</option>
                  <option value="Turkmenistan">Turkmenistan</option>
                  <option value="Turks and Caicos Islands">Turks and Caicos
                    Islands
                  </option>
                  <option value="Tuvalu">Tuvalu</option>
                  <option value="Uganda">Uganda</option>
                  <option value="Ukraine">Ukraine</option>
                  <option value="United Arab Emirates">United Arab Emirates
                  </option>
                  <option value="United Kingdom">United Kingdom</option>
                  <option value="United States Minor Outlying Islands">United
                    States Minor Outlying Islands
                  </option>
                  <option value="Uruguay">Uruguay</option>
                  <option value="Uzbekistan">Uzbekistan</option>
                  <option value="Vanuatu">Vanuatu</option>
                  <option value="Venezuela, Bolivarian Republic of">Venezuela,
                    Bolivarian Republic of
                  </option>
                  <option value="Viet Nam">Viet Nam</option>
                  <option value="Virgin Islands, British">Virgin Islands,
                    British
                  </option>
                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.
                  </option>
                  <option value="Wallis and Futuna">Wallis and Futuna</option>
                  <option value="Western Sahara">Western Sahara</option>
                  <option value="Yemen">Yemen</option>
                  <option value="Zambia">Zambia</option>
                  <option value="Zimbabwe">Zimbabwe</option>
                </select></div>
            </div>
        </div>
      </div>
      <input type="hidden" name="form_id" id="form_id_2468" value="2468"/>

      <p class="um_request_name">
        <label for="um_request_2468">Only fill in if you are not human</label>
        <input type="hidden" name="um_request" id="um_request_2468" class="input" value="" size="25" autocomplete="off"/>
      </p>

      <div class="um-col-alt">
        <p class="enhancements">
          <label for="agreement"><a href="<?= site_url() . '/terms-and-conditions/'; ?>" target="_blank">I
              agree to the
              terms of
              agreement.</a></label>
          <input id="agreement" onchange="terms(this);" type="checkbox">
        </p>

        <div class="um-center">
          <input type="submit" value="Upgrade" class="um-button" id="um-submit-btn" disabled=""/>
        </div>


        <div class="um-clear"></div>

      </div>

      <input type="hidden" name="ajaxy-umajax-mode" value="register"/>

      </form>

    </div>

  </div>
  <script>

    function terms(val) {
      var value = jQuery(val).is(':checked');

      if (value == true) {
//um-submit-btn
        jQuery('#um-submit-btn').prop("disabled", false);
      } else {
        jQuery('#um-submit-btn').prop("disabled", true);

      }
    }
  </script>
  <style type="text/css">
    .um-2468.um {
      max-width: 450px;
    }

    .enhancements {
      margin-bottom: 20px;
      font-size: 20px;
      text-align: center;
    }
  </style>
  <?php
  $output = ob_get_clean();
  return $output;
}

add_shortcode('mg_upgrade', 'mg_upgrade');
