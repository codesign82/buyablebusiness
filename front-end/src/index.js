import './styles/style.scss';
import {gsap} from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import header from './blocks/header_block';

import {initBlocks} from './blocks';
import {copyLink} from './scripts/general/copy-link';
import {accordionToggle} from './scripts/general/accordion-toggle';
import {breakLine} from './scripts/functions/breakLine';
import {stickySidebar} from './scripts/functions/stickySidebar';
import {getHeightOfViewPort} from './scripts/functions/getHeightOfViewPort';
import {setFormFunctionality} from './scripts/general/set-form-functionality';
import {scrollToHash} from './scripts/general/scroll-to-hash';

const reInvokableFunction = async (container = document) => {
  container.querySelector('header') && await header(container.querySelector('header'));
  await initBlocks(container);

  setFormFunctionality(container);
  copyLink(container);
  scrollToHash(container);
  accordionToggle(container);
  breakLine(container);
  stickySidebar(container);
  getHeightOfViewPort(container);

  ScrollTrigger.refresh(true);
};
let loaded = false;

async function onLoad() {
  gsap.config({
    nullTargetWarn: false,
  });
  if (document.readyState === 'complete' && !loaded) {
    loaded = true;
    gsap.registerPlugin(ScrollTrigger);

    await reInvokableFunction();
    //region wp-block-columns background position
    const wpCols = document.querySelector('.wp-block-columns');
    if (wpCols) {
      const wpColsStyle = wpCols.style;

      function wpColsStyleFun() {
        const wpColsBgPosition = (window.innerWidth - wpCols.offsetWidth) / 2;
        wpColsStyle.setProperty('--left', '-' + wpColsBgPosition + 'px');
      }

      wpColsStyleFun();
      window.addEventListener('resize', () => {
        wpColsStyleFun();
      })
    }
    //endregion wp-block-columns background position
    document.body.classList.add('loaded');
    if(window.innerWidth < 768){
      document.querySelector('.um.um-account').classList.add('uimob500')
    }

  }
}

onLoad();

document.onreadystatechange = function () {
  onLoad();
};
