import {gsap} from 'gsap';
import {debounce} from "../../functions/debounce";
import {getElementsForAnimation} from '../../functions/getElementsForAnimation';

function R(min, max) {
  return min + Math.random() * (max - min)
}

const confetties = [
  `data:image/svg+xml,%3Csvg width='28.5' height='23.25' viewBox='0 0 38 31' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0.271484 22.39C0.271484 22.39 16.3215 12.25 18.0115 0C18.0115 0 33.6415 0.84 37.0215 5.91C37.0215 5.91 29.8415 26.18 21.3915 29.99C21.3915 29.99 2.38148 31.26 0.271484 22.39Z' fill='%23ED7D2B'/%3E%3C/svg%3E`
  , `data:image/svg+xml,%3Csvg width='30' height='24.75' viewBox='0 0 40 33' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 23.99C0 23.99 17.2 13.13 19.01 0C19.01 0 35.76 0.909997 39.38 6.34C39.38 6.34 31.69 28.06 22.63 32.14C22.63 32.14 2.26 33.5 0 23.99Z' fill='%2386D1CD'/%3E%3C/svg%3E%0A`
  , `data:image/svg+xml,%3Csvg width='59.28' height='66.75' viewBox='0 0 104 89' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M73.724 34.0602C73.724 34.0602 66.9977 67.7939 31.4116 61.5228L21.5941 84.9063C21.5941 84.9063 42.487 89.9456 69.4083 52.7864C87.0309 28.4786 73.724 34.0602 73.724 34.0602Z' fill='%235685D8'/%3E%3Cpath d='M80.4136 0.914403C80.4136 0.914403 96.9213 29.0781 55.1704 69.3735C55.1704 69.3735 69.3854 52.7993 62.0371 20.7448L80.4136 0.914403Z' fill='%23739AF0'/%3E%3C/svg%3E%0A`
  , `data:image/svg+xml,%3Csvg width='32.25' height='26.25' viewBox='0 0 43 35' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M17.0232 29.0282C17.0232 29.0282 9.88664 23.7404 8.04753 15.8381C6.67819 9.97143 10.1115 2.61358 10.3914 2.28002L0.831028 0.411133C0.831028 0.411133 -2.0026 12.8655 2.79508 21.1455C4.26436 23.6815 8.5273 27.1789 11.271 28.4837C14.2895 29.921 17.0232 29.0282 17.0232 29.0282Z' fill='%236BB7BF'/%3E%3Cpath d='M33.6401 23.3141L42.3408 33.085C42.3408 33.085 17.7528 39.4959 3.69458 22.4214C3.69458 22.4214 24.3945 29.2934 33.6401 23.3141Z' fill='%2386D1CD'/%3E%3C/svg%3E%0A`
]


export function confettiFall(block) {
  const canvases = getElementsForAnimation(block, '.confetti-fall');
  for (const canvas of canvases) {
    
    const total = 10;
    
    const createConfetti = ()=>{
      const dimensions = canvas.getBoundingClientRect();
      canvas.innerHTML = '';
      for (let i = 0; i < total; i++) {
        const confetti = document.createElement('IMG');
        confetti.classList.add('confetti-img')
        confetti.src = confetties[~~(Math.random() * confetties.length)]
        canvas.appendChild(confetti);
        gsap.set(confetti, {x: R(0, dimensions.width), y: -10, z: R(-200, 200), scale: R(0.3, 1.2)});
        gsap.to(confetti, {duration: R(6, 15), y: dimensions.height + 10, ease: 'linear', repeat: -1, delay: -15});
        gsap.to(confetti, {
          duration: R(4, 8),
          x: '+=100',
          rotationZ: R(0, 180),
          repeat: -1,
          yoyo: true,
          ease: 'sine.inOut'
        });
        gsap.to(confetti, {
          duration: R(2, 8),
          rotationX: R(0, 360),
          rotationY: R(0, 360),
          repeat: -1,
          yoyo: true,
          ease: 'sine.inOut',
          delay: -5
        });
      }
    }
    
    createConfetti();
    window.addEventListener('resize', debounce(createConfetti, 200))
  }
}