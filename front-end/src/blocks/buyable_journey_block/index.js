import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";
import {gsap} from "gsap";
import {ScrollTrigger} from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const buyableJourneyBlock = async (block) => {
  let clickHand = gsap.timeline({repeat: -1, repeatDelay: .5, yoyo: true})
  clickHand
    .fromTo(block.querySelector(".click-hand"), {
      scale: 1,
      y: 0,
      transformOrigin: "center"
    }, {scale: .9, y: -5, transformOrigin: "center"})
    .fromTo(block.querySelector(".click-effect"), {
        scale: 0,
        transformOrigin: "center",
        ease: "back"
      }
      , {scale: 1, transformOrigin: "center", ease: "back"}, ">-.3.5")
    .to(block.querySelector(".click-hand"), {fill: '#C32729'}, "<")
    .to(block.querySelectorAll(".click-effect path"), {
      fill: '#C32729',
      stagger: .1
    }, "<")
    .to(block.querySelector(".click-hand-text"), {color: '#C32729'}, "<")


  animations(block);
  imageLazyLoading(block);
};

export default buyableJourneyBlock;

