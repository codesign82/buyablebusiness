import './index.html';
import './style.scss';
import {imageLazyLoading} from "../../scripts/functions/imageLazyLoading";
import {animations} from "../../scripts/general/animations";


const orchestrateTheFoundationBlock = async (block) => {

  // add block code here


    animations(block);
    imageLazyLoading(block);
};

export default orchestrateTheFoundationBlock;

