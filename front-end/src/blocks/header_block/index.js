import './index.html';
import './style.scss';
import {imageLazyLoading} from '../../scripts/functions/imageLazyLoading';
import {animations} from '../../scripts/general/animations';


export default async (header) => {

  document.querySelector(".menu").addEventListener("click", function () {
    document.querySelector("header").classList.toggle("active")
  })

  animations(header);
  imageLazyLoading(header);
};

