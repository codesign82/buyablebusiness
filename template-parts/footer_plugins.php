<?php
$title = get_field('title', 'options');
$phone_number = get_field('phone_number', 'options');
$support_email = get_field('support_email', 'options');
$site_link = get_field('site_link', 'options');
$show_social_icons = get_field('show_social_icons', 'options');
$social_links = get_field('social_links', 'options');
?>
<footer>
  <div class="container">
    <div class="footer-content">
      <?php if ($show_social_icons){ ?>
      <div class="social-icons">
        <?php if ($social_links['facebook']) { ?>
          <a class="icon facebook" href="<?= $social_links['facebook'] ?>">
            <svg height="30" viewBox="0 0 15 30" width="15" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path d="M9.708 8.96V6.6c0-1.151.767-1.418 1.306-1.418h3.312V.122L9.764.103c-5.063 0-6.214 3.775-6.214 6.19V8.96H.622v5.904h2.953v14.763h5.904V14.865h4.383l.212-2.318.326-3.586z"
                        fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
        <?php } ?>
        <?php if ($social_links['twitter']) { ?>
          <a class="icon twitter" href="<?= $social_links['twitter'] ?>">
            <svg height="24" viewBox="0 0 30 24" width="30" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path
                    d="M29.677 3.19a12.214 12.214 0 0 1-3.478.938 6.006 6.006 0 0 0 2.664-3.3 12.23 12.23 0 0 1-3.85 1.45A6.086 6.086 0 0 0 20.596.393c-3.345 0-6.057 2.67-6.057 5.963 0 .467.054.92.155 1.358a17.286 17.286 0 0 1-12.484-6.23 5.874 5.874 0 0 0-.822 2.997c0 2.068 1.072 3.894 2.695 4.963A6.115 6.115 0 0 1 1.338 8.7v.075c0 .763.15 1.491.415 2.161a6.047 6.047 0 0 0 4.445 3.688 6.253 6.253 0 0 1-1.597.21c-.39 0-.768-.039-1.14-.11.773 2.37 3.01 4.094 5.659 4.142a12.27 12.27 0 0 1-7.523 2.553c-.49 0-.971-.028-1.447-.083a17.368 17.368 0 0 0 9.287 2.678c9.469 0 15.289-6.56 16.823-13.365a16.37 16.37 0 0 0 .396-4.37 12.191 12.191 0 0 0 3.02-3.088z"
                    fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
        <?php } ?>
        <?php if ($social_links['linked_in']) { ?>
          <a class="icon linked-in" href="<?= $social_links['linked_in'] ?>">
            <svg height="31" viewBox="0 0 30 31" width="30" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path d="M.342 15.831v14.46h6.89V10.606H.342z" fill="#fefefe"/>
                </g>
                <g>
                  <path d="M3.788.766a3.445 3.445 0 1 0 0 6.89 3.445 3.445 0 0 0 0-6.89z" fill="#fefefe"/>
                </g>
                <g>
                  <path
                    d="M29.717 16.65c-.467-3.71-2.17-6.042-7.186-6.042-2.975 0-4.973 1.105-5.79 2.656h-.085v-2.656H11.17V30.29h5.735v-9.756c0-2.573.487-5.064 3.668-5.064 3.137 0 3.39 2.942 3.39 5.23v9.59h5.905V19.474h.001c0-1.008-.042-1.953-.15-2.823z"
                    fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
        <?php } ?>
        <?php if ($social_links['youtube']) { ?>
          <a class="icon youtube" href="<?= $social_links['youtube'] ?>">
            <svg height="22" viewBox="0 0 30 22" width="30" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path
                    d="M20.004 10.551l-7.974 4.308-.003-6.632V6.21l3.597 1.957zm8.367-8.898C27.248.428 25.989.42 25.414.35 21.28.04 15.085.04 15.085.04h-.015S8.871.04 4.741.351C4.164.42 2.908.428 1.782 1.653.899 2.587.611 4.711.611 4.711S.314 7.2.314 9.692v2.335c0 2.493.297 4.984.297 4.984s.288 2.122 1.171 3.055c1.126 1.227 2.602 1.187 3.257 1.315 2.362.236 10.038.31 10.038.31s6.203-.011 10.337-.321c.575-.072 1.834-.077 2.957-1.305.884-.933 1.172-3.054 1.172-3.054s.297-2.491.297-4.984V9.692c0-2.492-.297-4.982-.297-4.982s-.288-2.124-1.172-3.057z"
                    fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
        <?php } ?>
        <?php } ?>
      </div>
      <div class="links">
        <?php if ($title) { ?>
          <p> <?= $title ?> </p>
        <?php } ?>
        <?php if ($phone_number) { ?>
          <p class="p-phone"> ph:<a href="tel:+1.804.332.1307"> <?= $phone_number ?> </a></p>
        <?php } ?>
        <?php if ($support_email) { ?>
          <a href="<?= "mailto: $support_email"?>"> <?= $support_email ?> </a>
        <?php } ?>
        <?php if ($site_link) { ?>
          <a href=" <?= $site_link ?> "> <?= $site_link ?> </a>
        <?php } ?>

      </div>
    </div>
  </div>
</footer>
