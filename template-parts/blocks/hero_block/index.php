<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'hero_block';
$small_height = get_field('small_height');
$className = 'hero_block';
if($small_height){
  $className .= ' small-height';
}
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/hero_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region Buyablebusiness's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <a href="<?= site_url()?>" class="steps-move">
    <svg class="small-steps" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
         viewBox="0 0 155 125">
      <title>snake-svg</title>
      <path
        d="M113.67,87.81h35.19A1.12,1.12,0,0,1,150,88.93V123a1.11,1.11,0,0,1-1.11,1.11H113.67V87.81Z"
        fill="none"
        stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M113.67,87.81h35.19A1.12,1.12,0,0,1,150,88.93V123a1.11,1.11,0,0,1-1.11,1.11H113.67Z"
        fill="#fff"/>

      <a  class="red" id="gray">
        <path
          d="M113.67,87.81h35.19A1.12,1.12,0,0,1,150,88.93V123a1.11,1.11,0,0,1-1.11,1.11H113.67Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M113.66,87.81H77.36v36.3h36.3V108.82h0l2.36-2.36a.71.71,0,0,0,0-1l-2.36-2.36h0V87.81Z"
        fill="none"
        stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M113.66,87.81H77.36v36.3h36.3V108.82l2.36-2.36a.71.71,0,0,0,0-1l-2.36-2.36Z"
        fill="#fff"/>

      <a class="red" id="gray-2" data-name="gray">
        <path
          d="M113.66,87.81H77.36v36.3h36.3V108.82l2.36-2.36a.71.71,0,0,0,0-1l-2.36-2.36Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M77.38,87.81H41.07v36.3H77.38V108.82h0l2.35-2.36a.71.71,0,0,0,0-1l-2.35-2.36h0V87.81Z"
        fill="none"
        stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M77.38,87.81H41.07v36.3H77.38V108.82l2.35-2.36a.71.71,0,0,0,0-1l-2.35-2.36Z"
        fill="#fff"/>

      <a  class="red" id="gray-3" data-name="gray">
        <path
          d="M77.38,87.81H41.07v36.3H77.38V108.82l2.35-2.36a.71.71,0,0,0,0-1l-2.35-2.36Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M37.41,84.14H1.1a40,40,0,0,0,40,40V108.92h0l2.47-2.46a.71.71,0,0,0,0-1L41.07,103h0V87.81a3.66,3.66,0,0,1-3.66-3.67Z"
        fill="none" stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M37.41,84.14H1.1a40,40,0,0,0,40,40V108.92l2.47-2.46a.71.71,0,0,0,0-1L41.07,103V87.81a3.66,3.66,0,0,1-3.66-3.67Z"
        fill="#fff"/>

      <a  class="red" id="gray-4" data-name="gray">
        <path
          d="M37.41,84.14H1.1a40,40,0,0,0,40,40V108.92l2.47-2.46a.71.71,0,0,0,0-1L41.07,103V87.81a3.66,3.66,0,0,1-3.66-3.67Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M37.41,84.14H23.94a.42.42,0,0,1-.11.15L21.09,87a.71.71,0,0,1-1,0l-2.75-2.74a.58.58,0,0,1-.11-.15H1.11a40,40,0,0,1,40-40V80.48A3.66,3.66,0,0,0,37.41,84.14Z"
        fill="none" stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M37.41,84.14H23.94a.42.42,0,0,1-.11.15L21.09,87a.71.71,0,0,1-1,0l-2.75-2.74a.58.58,0,0,1-.11-.15H1.11a40,40,0,0,1,40-40V80.48A3.66,3.66,0,0,0,37.41,84.14Z"
        fill="#fff"/>

      <a class="red" id="gray-5" data-name="gray">
        <path
          d="M37.41,84.14H23.94a.42.42,0,0,1-.11.15L21.09,87a.71.71,0,0,1-1,0l-2.75-2.74a.58.58,0,0,1-.11-.15H1.11a40,40,0,0,1,40-40V80.48A3.66,3.66,0,0,0,37.41,84.14Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M77.38,80.48H41.07V65.21h0l-2.39-2.39a.71.71,0,0,1,0-1h0l2.39-2.39h0V44.17H77.38Z"
        fill="none"
        stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M77.38,80.48H41.07V65.21l-2.39-2.39a.71.71,0,0,1,0-1h0l2.39-2.39V44.17H77.38Z"
        fill="#fff"/>

      <a  class="red" id="gray-6" data-name="gray">
        <path
          d="M77.38,80.48H41.07V65.21l-2.39-2.39a.71.71,0,0,1,0-1h0l2.39-2.39V44.17H77.38Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M113.66,80.48H77.36V65.21h0L75,62.82a.71.71,0,0,1,0-1h0l2.39-2.39h0V44.17h36.3Z"
        fill="none" stroke="#ccc"
        stroke-miterlimit="20"/>
      <path
        d="M113.66,80.48H77.36V65.21L75,62.82a.71.71,0,0,1,0-1h0l2.39-2.39V44.17h36.3Z"
        fill="#fff"/>

      <a class="red" id="gray-7" data-name="gray">
        <path
          d="M113.66,80.48H77.36V65.21L75,62.82a.71.71,0,0,1,0-1h0l2.39-2.39V44.17h36.3Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M117.35,40.51h36.3a40,40,0,0,1-40,40V65.67a.67.67,0,0,1-.14-.11l-2.74-2.74a.71.71,0,0,1,0-1l2.74-2.74.14-.11V44.17a3.66,3.66,0,0,0,3.67-3.66Z"
        fill="none" stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M117.35,40.51h36.3a40,40,0,0,1-40,40V65.67a.67.67,0,0,1-.14-.11l-2.74-2.74a.71.71,0,0,1,0-1l2.74-2.74.14-.11V44.17a3.66,3.66,0,0,0,3.67-3.66Z"
        fill="#fff"/>

      <a  class="red" id="gray-8" data-name="gray">
        <path
          d="M117.35,40.51h36.3a40,40,0,0,1-40,40V65.67a.67.67,0,0,1-.14-.11l-2.74-2.74a.71.71,0,0,1,0-1l2.74-2.74.14-.11V44.17a3.66,3.66,0,0,0,3.67-3.66Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M117.35,40.51h15l2.58,2.58a.71.71,0,0,0,1,0h0l2.59-2.58h15.18a40,40,0,0,0-40-40v36.3a3.67,3.67,0,0,1,3.67,3.67Z"
        fill="none" stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M117.35,40.51h15l2.58,2.58a.71.71,0,0,0,1,0h0l2.59-2.58h15.18a40,40,0,0,0-40-40v36.3a3.67,3.67,0,0,1,3.67,3.67Z"
        fill="#fff"/>

      <a  class="red" id="gray-9" data-name="gray">
        <path
          d="M117.35,40.51h15l2.58,2.58a.71.71,0,0,0,1,0h0l2.59-2.58h15.18a40,40,0,0,0-40-40v36.3a3.67,3.67,0,0,1,3.67,3.67Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M113.66.54H77.36v36.3h36.3V21.91h0l2.73-2.73a.71.71,0,0,0,0-1l-2.73-2.72h0V.54Z"
        fill="none" stroke="#ccc"
        stroke-miterlimit="20"/>
      <path
        d="M113.66.54H77.36v36.3h36.3V21.91l2.73-2.73a.71.71,0,0,0,0-1l-2.73-2.72Z"
        fill="#fff"/>

      <a  class="red" id="gray-10" data-name="gray">
        <path
          d="M113.66.54H77.36v36.3h36.3V21.91l2.73-2.73a.71.71,0,0,0,0-1l-2.73-2.72Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M77.38.54H41.07v36.3H77.38V21.91h0l2.72-2.73a.71.71,0,0,0,0-1l-2.72-2.72h0V.54Z"
        fill="none" stroke="#ccc"
        stroke-miterlimit="20"/>
      <path
        d="M77.38.54H41.07v36.3H77.38V21.91l2.72-2.73a.71.71,0,0,0,0-1l-2.72-2.72Z"
        fill="#fff"/>

      <a  class="red" id="gray-11" data-name="gray">
        <path
          d="M77.38.54H41.07v36.3H77.38V21.91l2.72-2.73a.71.71,0,0,0,0-1l-2.72-2.72Z"
          fill="#e31e24"/>
      </a>
      <path
        d="M41.07.54H5.89A1.12,1.12,0,0,0,4.77,1.65V35.72a1.12,1.12,0,0,0,1.12,1.12H41.07V21.92h0l2.74-2.74a.69.69,0,0,0,0-1h0l-2.74-2.73h0V.54Z"
        fill="none" stroke="#ccc" stroke-miterlimit="20"/>
      <path
        d="M41.07.54H5.89A1.12,1.12,0,0,0,4.77,1.65V35.72a1.12,1.12,0,0,0,1.12,1.12H41.07V21.92l2.74-2.74a.69.69,0,0,0,0-1h0l-2.74-2.73Z"
        fill="#fff"/>

      <a  class="red active" id="gray-12" data-name="gray">
        <path
          d="M41.07.54H5.89A1.12,1.12,0,0,0,4.77,1.65V35.72a1.12,1.12,0,0,0,1.12,1.12H41.07V21.92l2.74-2.74a.69.69,0,0,0,0-1h0l-2.74-2.73Z"
          fill="#e31e24"/>
      </a>
    </svg>
  </a>
  <div class="buttons-group">
    <?php if (is_user_logged_in()) {
      $current_user = wp_get_current_user();
       $user_meta=get_userdata($current_user->ID);

	   $user_roles=$user_meta->roles;

	   if(in_array('buyable_upgraded',$user_roles)){
	   	$crown = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<path style="fill:#FFEA8A;" d="M512,120.242c0-17.11-13.92-31.03-31.03-31.03c-17.11,0-31.03,13.92-31.03,31.03
	c0,8.555,3.48,16.313,9.098,21.931l-94.431,94.433l-90.909-90.909c8.048-5.612,13.334-14.921,13.334-25.454
	c0-17.11-13.92-31.03-31.03-31.03s-31.03,13.92-31.03,31.03c0,10.533,5.286,19.842,13.334,25.454l-90.909,90.909l-94.431-94.433
	c5.618-5.618,9.098-13.376,9.098-21.931c0-17.11-13.92-31.03-31.03-31.03S0,103.132,0,120.242c0,14.428,9.911,26.551,23.273,30.009
	v272.536h465.455V150.252C502.089,146.794,512,134.67,512,120.242z"/>
<path style="fill:#FFDB2D;" d="M480.97,89.212c-17.11,0-31.03,13.92-31.03,31.03c0,8.555,3.48,16.313,9.098,21.931l-94.431,94.433
	l-90.909-90.909c8.048-5.612,13.334-14.921,13.334-25.454c0-17.11-13.92-31.03-31.03-31.03v333.576h232.727V150.252
	C502.089,146.794,512,134.67,512,120.242C512,103.132,498.08,89.212,480.97,89.212z"/>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>';
	   }else{
	   	$crown = '';
	   }
       ?>
      <p>Welcome <?= $current_user->user_firstname; ?><?= $crown; ?></p>
      <a class="upgrade" href="<?= site_url() . '/account' ?>">Account</a>
      <?php
	   if(!in_array('buyable_upgraded',$user_roles)){
      ?>
      <a class="upgrade" href="<?= site_url() . '/upgrade' ?>">Upgrade</a>
      <?php
  		}
      ?>
      <a class="logout-button" href="<?= site_url() . '/logout' ?>">Logout</a>
    <?php } else { ?>
      <a class="right-space" href="<?= site_url() . '/register' ?>">Sign up</a>
      <a href="<?= site_url() . '/login' ?>">Sign in</a>
    <?php } ?>
  </div>
  <?php if ($title['text']) { ?>
    <h1 class="headline-1"><?= $title['text'] ?>
      <?php if ($title['tm']) { ?>
        <sup><?= $title['tm'] ?></sup>
      <?php } ?>
    </h1>
  <?php } ?>
  <?php if ($description) { ?>
    <p class="paragraph"><?= $description ?></p>
  <?php } ?>
</div>
</section>


<!-- endregion Buyablebusiness's Block -->
