<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'best_seller_block';
$className = 'best_seller_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/best_seller_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title       = get_field( 'title' );
$description = get_field( 'description' );
?>
<!-- region Buyablebusiness's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <div class="best-seller-content">
    <div class="book">
      <div class="best-seller"> #1 best seller</div>
      <picture class="aspect-ratio">
        <img
          src="<?= get_template_directory_uri() ?>/front-end/src/images/2021_Steve_Preda_Buyable_landing_page2_v3.png"
          alt="">
      </picture>
    </div>
    <div class="about-book wysiwyg-block">
      <p><strong> A book on why you need a buyable business and how
          you can turn your lifestyle company into an asset that is
          as attractive for potential investors to acquire, as it is for
          you to keep.</strong>
        <br>
        <br>
        Steve distilled the experiences of his 20-year journey as a
        business owner, investment banker and business coach
        into a holistic program that helps you ﬁgure out what you
        really want, and how you can get there with the help of
        your business. The tricks, tips and mindsets of creating a
        buyable business are illustrated by over 100 real life
        stories from Steve’s career.
        <br>
        <br>
        Buy it on Amazon, download the ﬁrst chapter or chee
        the Buyability Tookit below.
      </p>
    </div>
  </div>
  <div class="buy-book">
    <a class="btn amazon-btn" href="#">
      <picture><img src="<?= get_template_directory_uri() ?>/front-end/src/images/amazon.png" alt=""></picture> </a>
    <a class="btn" href="#">Download the first chapter Toolkit</a>
    <a class="btn" href="#">Buyability Toolkit</a>
  </div>
</div>
</section>


<!-- endregion Buyablebusiness's Block -->
