<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'tools_content_block';
$className = 'tools_content_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/tools_content_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$number = get_field('number');
$title = get_field('title');

?>
<!-- region Buyablebusiness's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="step-icon-title">
  <p class="number"><?= $number ?></p>
  <div class="line"></div>
  <p class="big-title"><?= $title ?></p>
</div>
<?php if (have_rows('image_and_text')) {
  while (have_rows('image_and_text')) {
    the_row();
    $text = get_sub_field('text');
    $image = get_sub_field('image');
    $reversed = get_sub_field('reversed');
    $align_content = get_sub_field('align_content');
    if($reversed){
      $reversed = 'row-reverse';
    }
    ?>
    <div class="image-and-text" style="align-items:flex-<?=$align_content?>;flex-direction:<?=$reversed?>">
      <div class="paragraph"><?= $text ?></div>
      <picture>
        <img src="<?=$image['url'] ?>" alt="<?=$image['alt'] ?>">
      </picture>
    </div>
  <?php }
} ?>

</section>


<!-- endregion Buyablebusiness's Block -->
