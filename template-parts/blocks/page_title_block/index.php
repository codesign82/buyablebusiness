<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if ( ! empty( $block['anchor'] ) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'page_title_block';
$className = 'page_title_block';
if ( ! empty( $block['className'] ) ) {
  $className .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
  $className .= ' align' . $block['align'];
}
if ( get_field( 'is_screenshot' ) ) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/page_title_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
?>
<!-- region Buyablebusiness's Block -->
<?php general_settings_for_blocks( $id, $className, $dataClass ); ?>
<div class="container">
  <?php if ( have_rows( 'services' ) ) { ?>
    <div class="row">
      <?php while ( have_rows( 'services' ) ) {
        the_row();
        $icon_type = get_sub_field( 'icon_type' );
        $title     = get_sub_field( 'title' );
        $subtitle  = get_sub_field( 'subtitle' ); ?>
        <div class="col-6 col-md-6 col-lg-4 iv-st-from-bottom">
          <div class="card">
            <div class="icon-parent">
              <?php
              if ( $icon_type === 'svg_code' ) {
                echo get_sub_field( 'svg_code' );
              } else {
                $file = get_sub_field( 'file' );
                echo "<img src=" . $file['url'] . " alt=" . $file['alt'] . " />";
              }
              ?>
              <hr class="line">
            </div>
            <?php if ( $title ) { ?>
              <p class="headline-3"><?= $title ?></p>
            <?php } ?>
            <?php if ( $subtitle ) { ?>
              <p class="headline-3"><?= $subtitle ?></p>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  <?php } ?>
</div>
</section>


<!-- endregion Buyablebusiness's Block -->
