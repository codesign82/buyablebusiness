<?php
// Create id attribute allowing for custom "anchor" value.
$id = $block['id'];
if (!empty($block['anchor'])) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$dataClass = 'orchestrate_the_foundation_block';
$className = 'orchestrate_the_foundation_block';
if (!empty($block['className'])) {
  $className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
  $className .= ' align' . $block['align'];
}
if (get_field('is_screenshot')) :
  /* Render screenshot for example */
  echo '<img width="100%" height="100%" src="' . get_template_directory_uri() . '/template-parts/blocks/orchestrate_the_foundation_block/screenshot.png" >';

  return;
endif;

/****************************
 *     Custom ACF Meta      *
 ****************************/
$title = get_field('title');
$description = get_field('description');
?>
<!-- region Buyablebusiness's Block -->
<?php general_settings_for_blocks($id, $className, $dataClass); ?>
<div class="container">
  <div class="content-wrapper">
    <div class="steps-content">
      <div class="premium-step premium-step-1">
      </div>
      <div class="premium-step-wrapper">
        <div class="premium-step premium-step-2">
        </div>
        <div class="premium-step premium-step-3">
        </div>
      </div>
      <div class="premium-step-wrapper">
        <div class="premium-step premium-step-4">
        </div>
        <div class="premium-step premium-step-5">
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<!-- endregion Buyablebusiness's Block -->
