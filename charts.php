<?php

/*Template Name: Charts */

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/charts.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <title><?php echo get_bloginfo('name'); ?></title>

</head>
<body>
<header class="header">
  <div class="container">
    <div class="header-content">
      <div class="header-left">
        <div class="main-logo">
          <a href="https://stevepreda.com">
            <img width="" src="https://stevepreda.com/wp-content/uploads/2021/05/unnamed-1-1.jpg" alt="Logo">
          </a>
        </div>
        <div class="open-menu">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16">
            <path fill="#000" d="M0 15.999h24v-2.526H0zm0-6.737h24V6.736H0zM0 0v2.527h24V-.001z"></path>
          </svg>
        </div>
        <ul class="taps">
          <svg class="close-menu" enable-background="new 0 0 26 26" id="Слой_1" version="1.1" viewBox="0 0 26 26" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M14.0605469,13L24.7802734,2.2802734c0.2929688-0.2929688,0.2929688-0.7675781,0-1.0605469  s-0.7675781-0.2929688-1.0605469,0L13,11.9394531L2.2802734,1.2197266c-0.2929688-0.2929688-0.7675781-0.2929688-1.0605469,0  s-0.2929688,0.7675781,0,1.0605469L11.9394531,13L1.2197266,23.7197266c-0.2929688,0.2929688-0.2929688,0.7675781,0,1.0605469  C1.3662109,24.9267578,1.5576172,25,1.75,25s0.3837891-0.0732422,0.5302734-0.2197266L13,14.0605469l10.7197266,10.7197266  C23.8662109,24.9267578,24.0576172,25,24.25,25s0.3837891-0.0732422,0.5302734-0.2197266  c0.2929688-0.2929688,0.2929688-0.7675781,0-1.0605469L14.0605469,13z" fill="#1D1D1B"></path>
            </svg>
          <li class="tap ">
            <a target="" class="link" href="https://stevepreda.com/">
              Home </a>
          </li>
          <li class="tap ">
            <a target="" class="link" href="https://stevepreda.com/steve-preda/">
              About </a>
          </li>
          <li class="tap ">
            <a target="" class="link" href="https://buyablebusiness.com/">
              <i>Buyable</i> </a>
          </li>
          <li class="tap has-menu">
            <a target="" class="link" href="#">
              Tools </a>
            <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="angle-down" class="svg-inline--fa fa-angle-down fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
              <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
            </svg>
            <div class="drop-down">
              <div class="links-menu">
                <a target="" class="link" href="https://buyabilityassessment.com/">
                  Buyability Assessment </a>
                <a target="" class="link" href="https://magicnumbercalculator.com/">
                  Magic Number Calculator </a>
                <a target="" class="link" href="https://valueandgrowthcalculator.com/">
                  Value &amp; Growth Calculator </a>
              </div>
            </div>
          </li>
          <li class="tap has-menu">
            <a target="" class="link" href="#">
              Content </a>
            <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="angle-down" class="svg-inline--fa fa-angle-down fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
              <path fill="currentColor" d="M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"></path>
            </svg>
            <div class="drop-down">
              <div class="links-menu">
                <a target="" class="link" href="https://stevepreda.com/the-profserv-podcast/">
                  Management Blueprint Podcast </a>
                <a target="" class="link" href="https://stevepreda.com/succession-secrets-podcast/">
                  Succession Secrets Podcast </a>
                <a target="" class="link" href="https://stevepreda.com/steve-blog">
                  Blog </a>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="header-right">
        <a class="cta_button" href="https://calendly.com/stevepreda/25min">Let's Talk</a>
      </div>
    </div>
  </div>
</header>
<!-- Fields -->
<?php
$background_color = get_field('background_color');
$description = get_field('description');
$title = get_field('title');
$image = get_field('image');
?>
<section class="buyable" <?php if ($background_color) { ?> style="background: <?= $background_color ?>"  <?php } ?>>
  <div class="container">
    <div class="book-description">
      <?php if ($title) { ?>
        <h1 class="headline-1"><?= $title ?></h1>
      <?php } ?>
    <?php if ($description) { ?>
        <p class="paragraph"><?= $description ?></p>
    <?php } ?>
    </div>
    <?php if ($image) { ?>
      <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
    <?php } ?>
  </div>
</section>
<?php
if (have_rows('content')) {
  while (have_rows('content')) {
    the_row();
    $number = get_sub_field('number');
    $title = get_sub_field('title');
    ?>
    <section class="blocks-parent-section">
      <div class="container">
        <?php if ($number) { ?>
          <div class="section-number"><?= $number ?></div>
        <?php } ?>
        <?php if ($title) { ?>
          <h2 class="section-title"><?= $title ?></h2>
        <?php } ?>
      </div>
      <?php
      if (have_rows('inner_content')) {
        while (have_rows('inner_content')) {
          the_row();
          $background_color = get_sub_field('background_color');
          $padding = get_sub_field('padding');
          $title = get_sub_field('title');
          $title_bold = get_sub_field('title_bold');
          $image = get_sub_field('image');
          $sources = get_sub_field('sources');
          ?>
          <div class="block">
            <?php if ($title || $title_bold) { ?>
              <div class="container">
                <h3 class="block-title"><span><?= $title ?></span> <?= $title_bold ?></h3>
              </div>
            <?php } ?>
              <div class="block-content" style="<?php if ($background_color) { ?> background-color: <?=$background_color?>; <?php } ?>
              <?php if ($padding) { ?> padding: <?=$padding?>rem 0; <?php } ?>">
                <div class="container">
                  <div class="image-wrapper">
	                  <?php if ($image) { ?>
                    <img src="<?= $image['url'] ?>"
                         alt="<?= $image['alt'] ?>">
                    <?php } ?>
                    <?php if($sources){ ?>
                        <div class="wysiwyg sources"><?=$sources ?></div>
              <?php } ?>
                  </div>
                </div>
              </div>
          </div>
          <?php
        }
      }
      ?>
    </section>
    <?php
  }
}

get_footer();
?>

</body>
</html>