<?php
/*
 * Template Name: text Page
 * */


get_header();

?>

<?php while (have_posts()) : the_post(); ?>
  <div class="container page-wrapper dodo">
    <div class="text-page">
<!--      <h1 class="headline-2">--><?php //the_title() ?><!--</h1>-->
      <?php the_content(); ?>
    </div>
  </div>
<?php endwhile; ?>

<?php get_footer();
