<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="description" content="<?php if ( is_single() ) {
    single_post_title( '', true );
  } else {
    bloginfo( 'name' );
    echo " - ";
    bloginfo( 'description' );
  } ?>"/>
  <meta
    content="width=device-width, initial-scale=1.0, maximum-scale=5, minimum-scale=1.0"
    name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <script>
    const BiggerThanDesignWidth = 1920;
    const designWidth = 1440;
    const desktop = 1440;
    const tablet = 992;
    const mobile = 600;
    const sMobile = 375;
    const resizeHandler = function () {
      if (window.innerWidth >= BiggerThanDesignWidth) {
        document.documentElement.style.fontSize = `${10}px`;
        // document.documentElement.style.fontSize = `${9 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < BiggerThanDesignWidth && window.innerWidth >= desktop) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / desktop}px`;
      } else if (window.innerWidth < desktop && window.innerWidth >= tablet) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / designWidth}px`;
      } else if (window.innerWidth < tablet && window.innerWidth >= mobile) {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / tablet}px`;
      } else if (window.innerWidth < mobile && window.innerWidth >= sMobile) {
        document.documentElement.style.fontSize = `${10}px`;
      } else {
        document.documentElement.style.fontSize = `${10 * window.innerWidth / sMobile}px`;
      }
    };
    resizeHandler();
    window.addEventListener('resize', resizeHandler);
  </script>
  <style>
    /* latin */
    @font-face {
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 400;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* latin */
    @font-face {
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 700;
      font-display: swap;
      src: url(https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
  </style>
  <?php wp_head(); ?>
</head>
<!--preloader style-->
<style>
  body:not(.loaded) {
    opacity: 0;
  }

  body {
    transition: opacity .5s;
  }
</style>
<!--end preloader style-->
<!-- ACF Fields -->
<?php
$header_logo_image      = get_field( 'header_logo_image', 'options' );
$header_logo_svg        = get_field( 'header_logo_svg', 'options' );
$is_the_logo_png_or_svg = get_field( 'is_the_logo_png_or_svg', 'options' );
$container_width        = get_field( 'container_width', 'options' );
$is_white_header        = get_field( 'is_white_header', get_the_ID() );
$is_white_header        = $is_white_header ? 'white-header' : '';
?>
<?php if ( $container_width ) { ?>
  <style>
    @media screen and (min-width: 1650px) {
      .container, .wp-block-columns {
        max-width: <?=$container_width?> !important;
      }
    }
  </style>
<?php } ?>
<!-- END ACF -->
<body <?php body_class(); ?> data-barba="wrapper">
<!--  <div class="preloader"></div>-->
<main>
<!--  <header>-->
<!--    <a href="#" class="logo"><img src="--><?//= get_template_directory_uri() . '/front-end/src/images/steve-logo.jpg' ?><!--" alt='' /></a>-->
<!--    <div class="menu">-->
<!--      <span class="shape"></span>-->
<!--      <span class="shape"></span>-->
<!--    </div>-->
<!--    <nav>-->
<!--      <ul class="links-wrapper">-->
<!--        <li class="link-wrapper"><a href="https://buyablebusiness.com/login/">Sign In</a></li>-->
<!--        <li class="link-wrapper"><a href="https://buyablebusiness.com/register/">Sing Up</a></li>-->
<!--        <li class="link-wrapper"><a href="https://buyabilityassessment.com/">Buyability Assessment</a></li>-->
<!--      </ul>-->
<!--    </nav>-->
<!--  </header>-->
  <div class="page-transition">
<?php
if($wp_query->queried_object->post_name == 'account'){
  ?>
  <style>
    /*.um-avatar,.um-avatar-uploaded{*/
    /*  display: none!important;*/
    /*}*/
  </style>
  <?php
}
?>
