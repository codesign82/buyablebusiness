<?php
get_header();
global $post;
//$thumbnail_id = get_post_thumbnail_id( $post->ID );
//$image_alt    = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
$post_id    = get_the_ID();
$author_ID  = get_post_field( 'post_author', $post_id );
$authorData = get_userdata( $author_ID );
?>
<?php if ( have_posts() ): the_post(); ?>
  <div class="single-post-wrapper">
    <div class="container">
      <section class="blog_post_block" data-section-class="blog_post_block">
        <div class="blog-post-wrapper">
          <a class="btn has-border left-btn iv-st-from-top-f">
            <svg class="arrow" width="6" height="10" viewBox="0 0 6 10"
                 fill="none"
                 xmlns="http://www.w3.org/2000/svg">
              <path
                d="M0.154816 5.58203C-0.0209656 5.40625 -0.0209656 5.11328 0.154816 4.9375L3.94388 1.12891C4.13919 0.953125 4.43216 0.953125 4.60794 1.12891L5.05716 1.57812C5.23294 1.75391 5.23294 2.04688 5.05716 2.24219L2.04935 5.25L5.05716 8.27734C5.23294 8.47266 5.23294 8.76562 5.05716 8.94141L4.60794 9.39062C4.43216 9.56641 4.13919 9.56641 3.94388 9.39062L0.154816 5.58203Z"
                fill="currentColor"/>
            </svg>
            <?= __( 'News & Events', 'buyablebusiness' ) ?>
          </a>
          <h2 class="headline-1 word-up"><?php the_title(); ?></h2>
          <div class="category">
            <div class="author">
              <h6 class="headline-6 blue-color iv-st-from-bottom-f">
                <?php the_author_posts_link(); ?>
                , <?php echo $authorData->roles[0]; ?>
                <h6
                  class="headline-6 iv-st-from-bottom-f"><?php the_date( 'j F, Y' ); ?>
                  , <?php the_time( 'd.m' ); ?></h6>
            </div>
            <?php if ( in_category( 'events' ) ) {
              ?>
              <a href="#" class="btn btn-blue iv-st-from-bottom-f">
                <?= __( 'Register to Attend', 'buyablebusiness' ) ?></a>
            <?php } ?>
          </div>
          <div class="separator-wrapper" data-reveal-direction="left"></div>
          <picture class="aspect-ratio iv-a7a-zoom">
            <img data-src="<?php thumbnail_url(); ?>"
                 alt="blog image"/>
          </picture>
          <div
            class="blog-post-description paragraph iv-st-from-bottom-f"><?php the_content(); ?></div>
        </div>
      </section>
    </div>
  </div>
<?php endif; ?>
<?php
get_footer();
